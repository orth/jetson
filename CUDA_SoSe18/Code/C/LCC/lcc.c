/*
 * lcc.c
 *
 *  Created on: May 6, 2018
 *      Author: chris
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


typedef struct List {
	int size;
	double data[1000000];
} List;

//3 CSV Files in the directory you are working with
static char sourcefile[20]="export.csv";
static char matchingfile[20]="matching.csv";
static char resultfile[20]="Ergebnisse.csv";

//amount of sources in the matchingfile
static int amountofsources=6;
//amount of lines in the sourcefile
static int amountoflines=17284;
//amount of lines in the matchingfile
static int amountoflinesM=409;
//amount of speed data after acceleration
static int amountspeed=10;
//amount of consumption data after acceleration
static int amountconsumption=10;



//to read in data int colum = column from CSV
double convertStringToDouble(char * str, int column) {
	int length = strlen(str);
	int i;
	int k = 0;
	char send[40];
	int semicolon = 0;

	for (i = 0; i < length; i++) {
		if (str[i] == ',') {
			str[i] = '.';
		}
	}

	for (i = 0; i < length; i++) {
		if (str[i] == ';')
			semicolon++;

		if (semicolon == column) {
			if (column > 0)
				i++;

			while (str[i] != ';') {

				send[k] = str[i];

				i++;
				k++;
				if (k > 30) {
					break;
				}

			}

			//printf("\nATOF SEND IS %f", atof(send));
			return atof(send);

		}
	}
	//printf("\nsend is %f", atof(send));
	return atof(send);
}

struct List getLongi(char * x, int source) {
	FILE *CSV_Matching;
	CSV_Matching = fopen(x, "r");
	List *y = malloc(sizeof *y);
	char dataStringh[1024];
	int i = 0;
	while (fgets(dataStringh, 1024, CSV_Matching) != NULL) {
		y->data[i] = convertStringToDouble(dataStringh, source);
		i++;
		if (i > 5)
			break;
	}
	return *y;
}

struct List getLatti(char * x, int source) {
	FILE *CSV_Matching;
	CSV_Matching = fopen(x, "r");
	List *y = malloc(sizeof *y);
	char dataStringh[1024];
	int i = 0;
	while (fgets(dataStringh, 1024, CSV_Matching) != NULL) {
		y->data[i] = convertStringToDouble(dataStringh, source);
		i++;
		if (i > 5)
			break;
	}
	return *y;
}

//checkExport
/*
 * Make sure Export CSV file is in the directory
 * 3rd parameter int source is the source from the export file -1
 * since source 1 is in column zero
 * 4th parameter int speedx: if 0: checkExport returns a struct List with
 * the first 10 speed data after first acceleration(speed>0) and if you
 * call the function with speed=1 it will return the first 10 fuel consumption
 * data after first acceleration
 * 5th parameter int round can be ignored ->set always 0
 *
 * checkExport() goes through the hole sourcefile CSV in the directory and checks whether
 * latti and longi parameters given to the function do exactly match with those from the sourcefile
 * if there is no match it will go through the file a second time but thistime ignoring the last digit
 * of the longi value.
 */
struct List checkExport(double latti, double longi, int source, int speedx,
		int round) {
	FILE *CSV_Export;
	CSV_Export = fopen(sourcefile, "r");
	List *speed = malloc(sizeof *speed);
	char dataStringh[1024];
	int i = 0;
	int k = 0;
	double temp = 0;
	double tempv = 0;
	double templatti = 0;
	double templongi = 0;
	int gefunden = 0;


	while (fgets(dataStringh, 1024, CSV_Export) != NULL) {

		temp = (convertStringToDouble(dataStringh, 36 + source));
		tempv = (convertStringToDouble(dataStringh, 48 + source));

		templatti = (convertStringToDouble(dataStringh, 0 + source));
		templongi = (convertStringToDouble(dataStringh, 6 + source));



		if ((templatti == latti) && (templongi == longi)) {
			gefunden = 1;
		}

		if (gefunden == 1) {
			if (temp > 0) {
				if (speedx == 0) {
					speed->data[k] = temp;
				}
				if (speedx == 1) {
					speed->data[k] = tempv;
				}
				k++;
				if (k > 10)
					break;
			}
		}

		i++;
		//this triggers when Export file is checked and no match was found
		//lthis time last digit of longi value is ignored
		if (i > amountoflines && gefunden==0) {
			if (k == 0 && round == 0) {
				//printf("runde2");
				int z = 0;
				CSV_Export = fopen(sourcefile, "r");
				while (fgets(dataStringh, 1024, CSV_Export) != NULL) {

					temp = (convertStringToDouble(dataStringh, 36 + source));

					tempv = (convertStringToDouble(dataStringh, 48 + source));

					templatti =
							(convertStringToDouble(dataStringh, 0 + source));
					templongi =
							(convertStringToDouble(dataStringh, 6 + source));

					if ((templatti == latti)
							&& ((int) (templongi * 10000)
									== (int) (longi * 10000))) {
						//printf("hier in %i\n",i);
						//printf("templatti: %f, templongi: %f\n",templatti,templongi);
						gefunden = 1;
					}

					if (gefunden == 1) {
						if (temp > 0) {
							if (speedx == 0) {
								speed->data[k] = temp;
							}
							if (speedx == 1) {
								speed->data[k] = tempv;
							}

							//printf("temp: %f\n",speed->data[k]);
							k++;
							//
							if (k > 10)
								break;
						}
					}
					z++;
					if (z > amountoflines) {
						printf("nix");
						break;
					}
				}

			} else {
				break;
			}
		}

	}

	return *speed;

}


void printSpeed(struct List *lattix, struct List *longix, int no,
		struct List *lattiy, struct List *longiy, int source1, int source2) {
	printf("\n");
	List *dumpp = malloc(sizeof *dumpp);
	//*dumpp = checkExport(lattix->data[1], longix->data[1], 0, 0);
	printf("\nPAAR 1_________________________________________________\n");
	//printf("\nQuelle%i: latti: %f  longi: %f\n",source1, lattix->data[no],longix->data[no]);
	//*dumpp = checkExport(lattix->data[no],longix->data[no],source1,0);

	//for(int i=0;i<=10;i++){
	//	printf("   %.0f ",dumpp->data[i]);
	//}
	/*printf("\nQuelle%i: latti: %f  longi: %f\n",source2, lattix->data[no],longix->data[no]);
	 *dumpp = checkExport(lattiy->data[no],longiy->data[no],source1,0);

	 for(int i=0;i<=10;i++){
	 printf("   %.0f ",dumpp->data[i]);
	 }*/
}

double durschnittsVerbrauch(struct List *x) {
	double verbrauch = 0;
	for (int i = 0; i < amountconsumption; i++) {
		verbrauch += x->data[i];
	}
	verbrauch /= amountconsumption;
	return verbrauch;
}

double Strecke(struct List *x) {
	double strecke = 0;
	double speed = 0;
	for (int i = 0; i < amountspeed; i++) {
		speed = x->data[i];
		speed /= 3.6;
		strecke += speed;
	}
	return strecke;
}

int main(int argc, char **argv) {

	//1+2
	FILE *CSV_Matching;
	CSV_Matching = fopen(matchingfile, "r");
	FILE *CSV_Ergebnisse;
	CSV_Ergebnisse = fopen(resultfile, "w");
	List *latti = malloc(sizeof *latti);  //Zeile 0+1
	List *longi = malloc(sizeof *longi);   //Zeile 2+3
	List *dump = malloc(sizeof *dump);
	List *dumpV = malloc(sizeof *dumpV);
	char dataStringh[1024];
	double tempdata;
	double verbrauch = 0;
	double strecke = 0;
	if (NULL == CSV_Matching) {
		fprintf(stderr, "Fehler beim Oeffnen ...\n");

		return EXIT_FAILURE;
	}

	int k = 0;
	int i = 0;
	int search = 0;
	//CSV Kopfzeilen
	//Change here if more than first 10 data of speed or consumtion
	fprintf(CSV_Ergebnisse,
			"Quelle;Lattitude;Longitude;Strecke;o-Verbrauch;Speed1;Speed2;Speed3;Speed4;Speed5;Speed6;Speed7;Speed8;Speed9;Speed10"
			"Verbrauch1;Verbrauch2;Verbrauch3;Verbrauch4;Verbrauch5;Verbrauch6;Verbrauch7;Verbrauch8;Verbrauch9;Verbrauch10;\n");

	while (fgets(dataStringh, 1024, CSV_Matching) != NULL) {

		if (convertStringToDouble(dataStringh, 0) > 0) {
			//printf(" %f ", convertStringToDouble(dataStringh, 0));
			//printf(" %f\n", convertStringToDouble(dataStringh, 1));

		}
		while (search < amountofsources) {

			if (convertStringToDouble(dataStringh, search) > 0) {
				printf("%d", search);
				printf(" %f\n", convertStringToDouble(dataStringh, search));

				printf(" %f\n", convertStringToDouble(dataStringh, search + amountofsources));

				*dump = checkExport(convertStringToDouble(dataStringh, search),
						convertStringToDouble(dataStringh, search + amountofsources), search,
						0, 0);
				for (int i = 0; i < amountspeed; i++) {
					printf("   %.0f ", dump->data[i]);
				}
				strecke = Strecke(dump);
				printf("\n");

				*dumpV = checkExport(convertStringToDouble(dataStringh, search),
						convertStringToDouble(dataStringh, search + amountofsources), search,
						1, 0);
				for (int i = 0; i < amountconsumption; i++) {
					printf("   %.2f ", dumpV->data[i]);
				}

				verbrauch = durschnittsVerbrauch(dumpV);
				printf("\nVerbrauch: %.2f\n", verbrauch);

				//Write in CSV
				//Quelle
				fprintf(CSV_Ergebnisse, "%d;", search + 1);
				//Latti +Longi
				fprintf(CSV_Ergebnisse, "%f;",
						convertStringToDouble(dataStringh, search));
				fprintf(CSV_Ergebnisse, "%f;",
						convertStringToDouble(dataStringh, search + amountofsources));
				//Strecke+Verbrauch
				fprintf(CSV_Ergebnisse, "%.2f;%.2f;", strecke, verbrauch);
				//Speed 1-10
				for (int i = 0; i < amountspeed; i++) {
					fprintf(CSV_Ergebnisse, "%.0f;", dump->data[i]);
				}
				//Verbrauch 1-10
				for (int i = 0; i < amountconsumption; i++) {
					fprintf(CSV_Ergebnisse, "%.2f;", dumpV->data[i]);
				}
				//NextRow
				fprintf(CSV_Ergebnisse, "\n");
			}
			search++;

		}
		k++;
		search = 0;
		if (k > amountoflinesM) {
			break;
		}

	}

	return EXIT_SUCCESS;
}
