/*
 * ReadFile.h
 *
 *  Created on: Apr 28, 2018
 *      Author: Florian Anders
 */

#ifndef READFILE_H_
#define READFILE_H_

double** readFile(char *file, char* mode, char valueSeparator);
void getValues(char line[], char*** dataC, int data_x_pos, char valueSeparator);


#endif /* READFILE_H_ */



