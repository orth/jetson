/**
 * Author: Florian Anders
 * Created on 29.04.2018
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ReadFile.h"
#include "ParseFile.h"

#define DATA_ROWS 10796
#define DATA_COLUMNS 6
#define VALUE_LENGTH 20

/**
 * Function that separates each line into multiple values
 * and stores them in the a 3 dim. char array
 */
void getValues(char line[], char*** dataC, int data_x_pos, char valueSeparator){
    int i = 0,z = 0,y = 0, j;
    char tmp[20];

    while(1){

        if((line[i] == valueSeparator || line[i] == '\0') && z != 0){
            for(j = 0; j < z; j++){
                dataC[data_x_pos][y][j] = tmp[j];
            }
            for(;z < VALUE_LENGTH; z++){
                dataC[data_x_pos][y][z] = ' ';
            }
            y++;
            z = 0;
        }else{
            tmp[z++]= line[i];
        }
        if(line[i] == '\0'){
        	break;
        }
        i++;
    }
}
/**
 * Function that reads text-files and
 * returns the values in a 2 dim.double array
 */
double** readFile(char *filePath, char valueSeparator){

    printf("ReadFile...\n");
    int i, j;
    FILE* fp;
    char line[100];
    char*** dataC = (char***)malloc(DATA_ROWS* sizeof(char**));

    for(i = 0; i < DATA_ROWS; i++){
        dataC[i] = (char**)malloc(sizeof(char*) * DATA_COLUMNS);
        for(j = 0; j < DATA_COLUMNS; j++){
            dataC[i][j] = (char*)malloc(sizeof(char) * VALUE_LENGTH);
        }
    }
    fp = fopen(filePath, "r");
    if (fp == NULL){
        printf("\n---file not found---\n");
        exit(EXIT_FAILURE);
    }
    int data_x_pos = 0;
    while(fgets(line, sizeof(line), fp) != NULL){

        getValues(line, dataC, data_x_pos++, valueSeparator);
    }
    fclose(fp);
    return parseFile(dataC);
}
