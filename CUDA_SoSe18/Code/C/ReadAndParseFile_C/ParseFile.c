/**
 * Author Florian Anders
 * Created on 03.05.2018
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ParseFile.h"

#define DATA_ROWS 10796
#define DATA_COLUMNS 6
#define VALUE_LENGTH 20

double** parseFile(char*** dataC){

    printf("ParseFile started...\n");
    int i,j;
    double** dataD = (double**)malloc(DATA_ROWS* sizeof(double*));
    for(i = 0; i < DATA_ROWS; i++){
        dataD[i] = (double*)malloc(sizeof(double*) * DATA_COLUMNS);
    }
    int zeile = 0;
    for(i = 0; i < DATA_ROWS; i++){
        printf("\nzeile = %d...", zeile++ );
        for(j = 0; j < DATA_COLUMNS; j++){
            dataD[i][j] = atof(dataC[i][j]);
            printf(" %f ", dataD[i][j]);
        }
    }
    printf("\n");

    free(dataC);

    return dataD;
}
