/*
 *  bmz.cuh
 *
 *  Created on: Mai 1, 2018
 *  Author: Manuel Kaufmann
 *  Webpage: www.manuel-kaufmann.de
 *
 *  This is the implementation of bmz calculation. The bmz result is a 3 dimension
 *  array. The dimensions are range class, mean class and the used columns from
 *  data file. For noise canceling was used the stencil algorithm.
 *
 *  Use for configuration the header file constants.h.
 */

#include <stdio.h>
#include <stdlib.h>
#include "bmz.cuh"
#include "constants.h"

__device__ double getMedianBetweenMinimaMaxima(double y1, double y2){ return (double)(y2 - y1)/2;}
__device__ int getRangeClass(double minimum, double maximum){ return maximum - minimum;}
__device__ double getMeanClass(double rangeClass, double minima){ return rangeClass + minima;}

__device__ void countRangeClassAndMeanClass(int col){
	int range_class, median_class = 0;
	double minima, maxima = 0.0;
	int lockMinima = 1;

	for(int i = 0; i < DATA_ROWS; i++){
		if(stencil[i-1][col] < stencil[i][col]){
			if(lockMinima ==1) {
				minima = stencil[i-1][col];
				lockMinima = 0;	// minima is found. Set to 0.
			}
			maxima = stencil[i][col];
		}else if (stencil[i-1][col] > stencil[i][col]  && lockMinima == 0){
			range_class = getRangeClass(minima, maxima);
			median_class = getMeanClass(getMedianBetweenMinimaMaxima(minima, maxima), minima);
			bmzResult[range_class][median_class][col]++;
			maxima = 0.0;
			minima = 0.0;
			lockMinima = 1; // Unset minima lock. Set to 1.
		}
	}
}

__device__ void stencilAlgo(int col, double** readResult){
	double stencilVar = 0.0;
	int div_counter=0;
	for(int r=0; r < DATA_ROWS; r++){
		for(int sr=-STENCIL_RADIUS; sr <= STENCIL_RADIUS; sr++){
			if(r>=0 && r<=STENCIL_RADIUS){							// First Elements
				if(sr >= -r){
					stencilVar += readResult[r+sr][col];
					div_counter++;
				}
			}else if(r<=DATA_ROWS && r>=DATA_ROWS-STENCIL_RADIUS){	// Last Elements
				if(sr < (DATA_ROWS-r)){
					stencilVar += readResult[r+sr][col];
					div_counter++;
				}
			}else{													// Elements between
				stencilVar += readResult[r+sr][col];
				div_counter++;
			}
		}
		stencil[r][col] = stencilVar/div_counter;
		div_counter=0;
		stencilVar=0;
	}
}

__global__ void bmz(double** parseResult, int*** resultHost3D){
	stencilAlgo(threadIdx.x, parseResult);
	countRangeClassAndMeanClass(threadIdx.x);
	for(int i=0; i < CLASS_RANGE; i++){
		for(int j=0; j < CLASS_RANGE; j++){
			resultHost3D[i][j][threadIdx.x] = bmzResult[i][j][threadIdx.x];
		}
	}
	printf("BMZ calculation for thread %d is finished!\n", threadIdx.x);
}
