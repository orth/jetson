/*
 *  bmz.cuh
 *
 *  Created on: Mai 1, 2018
 *  Author: Manuel Kaufmann
 *  Webpage: www.manuel-kaufmann.de
 *
 *  This is the implementation of bmz calculation. The bmz result is a 3 dimension
 *  array. The dimensions are range class, mean class and the used columns from
 *  data file. For noise canceling was used the stencil algorithm.
 *
 *  Use for configuration the header file constants.h.
 */
#include "constants.h"
#ifndef BMZ_H_
#define BMZ_H_

/**
 * Device Arrays
 */
__device__ double stencil[DATA_ROWS][DATA_COLUMNS];
__device__ int bmzResult[CLASS_RANGE][CLASS_RANGE][MAX_THREADS];

/**
 * Stencil algorithm
 */
__device__ void stencilAlgo(int col, double** readResult);

/**
 * Counting range class and median class
 */
__device__ void countRangeClassAndMeanClass(int col);

/**
 * Function to get the actual range class
 */
__device__ int getRangeClass(double minimum, double maximum);

/**
 * Function to get the mean class
 */
__device__ double getMeanClass(double rangeClass, double minima);

/**
 * get median between two points (minima and maxima)
 */
__device__ double getMedianBetweenMinimaMaxima(double y1, double y2);

/**
 * Kernel function for BMZ Calculation
 */
__global__ void bmz(double** parseResult, int*** bmzResult3D);

#endif /* BMZ_H_ */
