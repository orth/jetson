/*
 *  readFile.h
 *
 *  Created on: Apr 28, 2018
 *      Author: cuda
 */

#ifndef READFILE_H_
#define READFILE_H_

#include "parseFile.h"
#include "constants.h"

/*
 * 2 dimensional pointer array
 * Stores the parsed file
 */
extern double** list;

/**
 * Function that separates each line into multiple values
 * and stores them in the data matrix
 */
void getValues(char line[], char*** dataC, int data_x_pos);

/**
 * Function that reads text-files
 */
char*** readFile(char *file, char *mode);

//void getFileReadResult(char *pathToFilem, double** parseResult);

double** getFileReadResult(char *pathToFilem, double** parseResult);

#endif /* READFILE_H_ */



