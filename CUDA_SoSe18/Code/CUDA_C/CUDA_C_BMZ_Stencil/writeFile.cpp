/*
 *  writeFile.h
 *
 *  Created on: Mai 13, 2018
 *  Author: Manuel Kaufmann
 */

#include <stdio.h>
#include "writeFile.h"
#include "constants.h"

/*
 * Writes the results of BMZ calculation in file
 */
int writeFile(char *pathToFile, int*** resultHost3D){

	FILE *fileWriter;
	fileWriter = fopen(pathToFile, "w+");

	if(fileWriter){
		fprintf(fileWriter, "BMZ class count over the Coloumns 1 - %d\n", MAX_THREADS);
		fprintf(fileWriter, "Range Class;Mean Class;Column;Count\n");

		/*
		* Parameters are Class, Col1, Col2, Col3, Col4, Col5, Col6
		*/
		for(int i=0; i < CLASS_RANGE; i++){
			for(int j=0; j<CLASS_RANGE; j++){
				for(int k=0; k < MAX_THREADS; k++){
					if(resultHost3D[i][j][k] > 0){
						fprintf(fileWriter, "%d;%d;%d;%d\n", i, j, k, resultHost3D[i][j][k]);
					}
				}
			}
		}

		fclose(fileWriter);
		return 1;
	}else{
		return 0;
	}
}
