/*
 * ParseFile.h
 *
 *  Created on: May 03, 2018
 *      Author: Florian Anders
 */
#ifndef PARSEFILE_H_
#define PARSEFILE_H_

double** parseFile(char*** dataC);


#endif /* PARSEFILE_H_ */


