/*
 *  constants.cuh
 *
 *  Created on: Mai 1, 2018
 *  Author: Manuel Kaufmann
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_


#define DATA_ROWS 10796		//Maximum amount of data rows in file
#define DATA_COLUMNS 6		//Maximum amount of data columns in file
#define RECORD_LENGTH 20	//Maximum amount of chars (record length) in read file
#define CLASS_RANGE 120		//Class Range reflects the maximum speed
#define MAX_THREADS 6		//Maximum used threads
#define MAX_BLOCKS 1		//Maximum used blocks
#define STENCIL_RADIUS 3	//Set Radius

#endif /* CONSTANTS_H_ */
