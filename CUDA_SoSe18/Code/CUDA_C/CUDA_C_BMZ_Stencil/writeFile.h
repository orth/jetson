/*
 *  writeFile.h
 *
 *  Created on: Mai 13, 2018
 *  Author: Manuel Kaufmann
 */

#ifndef WRITEFILE_H_
#define WRITEFILE_H_

/*
 * Writes the results of BMZ calculation in file
 */
int writeFile(char *pathToFile, int*** bmzResult3D);

#endif /* WRITEFILE_H_*/
