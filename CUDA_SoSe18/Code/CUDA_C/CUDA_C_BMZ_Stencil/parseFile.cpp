/**
 * Author Florian Anders
 * Created on 03.05.2018
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "parseFile.h"

#define DATA_ROWS 10796
#define DATA_COLUMNS 6
#define RECORD_LENGTH 20


double** parseFile(char*** dataC){
	printf("Function parseFile started\n");
    int i,j;
    double** dataD = (double**)malloc(DATA_ROWS* sizeof(double*));
    for(i = 0; i < DATA_ROWS; i++){
        dataD[i] = (double*)malloc(sizeof(double*) * DATA_COLUMNS);
    }
    int anzahl = 0;
    for(i = 0; i < DATA_ROWS; i++){
        //printf("zeile = %f\n", anzahl++);
        for(j = 0; j < DATA_COLUMNS; j++){
            dataD[i][j] = atof(dataC[i][j]);
            //printf("\ndataD: %", dataD[i][j]);

        }
    }
    printf("\n");
    return dataD;
}
