/**
 * Author Florian Anders
 * Created on 29.04.2018
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readFile.h"

/**
 * Function that separates each line into multiple values
 * and stores them in the data matrix
 */

void getValues(char line[], char*** dataC, int data_x_pos){
    int i = 0,z = 0,y = 0, j;
    char tmp[20];

    while(1){
        //printf("line[%i] = %i\n", i, line[i] );
        if((line[i] == ';' || line[i] == '\0') && z != 0){
            for(j = 0; j < z; j++){
                dataC[data_x_pos][y][j] = tmp[j];
            }
            for(;z < RECORD_LENGTH; z++){
                dataC[data_x_pos][y][z] = ' ';
            }
            y++;
            z = 0;
        }else{
            tmp[z++]= line[i];
        }
        if(line[i] == '\0'){
        	break;
        }
        i++;
    }
}

/**
 * Function that reads text-files
 */

char*** readFile(char *file, char *mode){

    printf("ReadFile...\n");
    int i, j;
    FILE* fp;
    char line[100];
    char*** dataC = (char***)malloc(DATA_ROWS* sizeof(char**));

    for(i = 0; i < DATA_ROWS; i++){
        dataC[i] = (char**)malloc(sizeof(char*) * DATA_COLUMNS);

        for(j = 0; j < DATA_COLUMNS; j++){
            dataC[i][j] = (char*)malloc(sizeof(char) * RECORD_LENGTH);

        }
    }

    fp = fopen(file,mode);

    if (fp == NULL){
        exit(EXIT_FAILURE);

    }
    int data_x_pos = 0;
    while(fgets(line, sizeof(line), fp) != NULL){
        //printf("fgets %i\n", data_x_pos);
        getValues(line, dataC, data_x_pos++);
    }
    fclose(fp);


    return dataC;
}


double** getFileReadResult(char *pathToFile, double** parseResult){

	char*** dataC = readFile(pathToFile, "r");

	return parseFile(dataC);
}
