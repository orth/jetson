/**
 * CUDA BMZ Project SoSe 2018 by
 * Manuel Kaufmann
 * Florian Anders
 * Lmar Niazman
 */

#include <stdio.h>
#include "bmz.cuh"
#include "readFile.h"
#include "writeFile.h"
#include "constants.h"
#include "cuda_runtime.h"
#include "vector_types.h"
#include <cuda_runtime_api.h>
#include "cuda_runtime.h"

/*
 * Print the BMZ results
 */
void printBmzResult(int*** resultHost3D){
	printf("RC\tMC\tCol\tCount\n");
	for(int i=0; i < CLASS_RANGE;i++){
		for(int j=0; j < CLASS_RANGE; j++){
			for(int k=0; k < MAX_THREADS; k++){
				if(resultHost3D[i][j][k] > 0){
					printf("%d\t%d\t%d\t%d \n", i, j, k, resultHost3D[i][j][k]);
				}
			}
		}
	}
}


int main(void){
	/*
	 * Declare pointer arrays
	 */
	int*** resultHost3D;	// Stores the calculated BMZ results
	double** listDevice;	// Stores the read file results
	double** listHost;		// Stores the read file results

	/*
	 * Allocate 3 dim pointer resultHost3D
	 */
	cudaMallocManaged(&resultHost3D, sizeof(int*) * CLASS_RANGE);
	for(int i = 0; i < CLASS_RANGE; i++){
		cudaMallocManaged(&resultHost3D[i], sizeof(int*) * CLASS_RANGE);
		for(int j = 0; j < CLASS_RANGE; j++){
			cudaMallocManaged(&resultHost3D[i][j] , sizeof(int*) * MAX_THREADS);
		}
	}


	/*
	 * Allocate 2 dim pointer listHost
	 */
	listHost = (double**)malloc(DATA_ROWS* sizeof(double**));
	for(int i = 0; i < CLASS_RANGE; i++){
		listHost[i] = (double*)malloc(sizeof(double*) * DATA_COLUMNS);
	}

	/*
	 * Path to file on remote machine
	 */
	char pathToFile[]= "/tmp/Team007/bmz_speed.txt";

	/*
	 * Store the results of file reading and parsing in a 2 dim pointer array
	 */
	listHost = getFileReadResult(pathToFile, listHost);

	/*
	 * Allocate device pointer
	 * cudaMallocManaged allows direct access of GPU data from the host
	 */
	cudaMallocManaged(&listDevice, sizeof(double*) * CLASS_RANGE);

	/*
	 * Allocate device pointer
	 * Copy host list to device list
	 */
	for(int i = 0; i < DATA_ROWS; i++){
		cudaMallocManaged(&listDevice[i], sizeof(double) * DATA_COLUMNS);
		for(int j=0; j < DATA_COLUMNS; j++){
			listDevice[i][j]=listHost[i][j];
		}
	}

	/*
	 * Define variables for performance measuring
	 */
	float milliseconds = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start);

	/*
	 * Kernel setup
	 */
	dim3 blocks1D(MAX_BLOCKS);
	dim3 threadsPerBlock1D(MAX_THREADS);


	/*
	 * Kernel invocation
	 */
	bmz<<<blocks1D, threadsPerBlock1D>>>(listDevice, resultHost3D);

	/*
	 * Waits until all preceding commands in all streams of all host threads have completed
	 */
	cudaDeviceSynchronize();

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milliseconds, start, stop);

	printf("Elapsed time: %.5f seconds\n", milliseconds/1000);

	printBmzResult(resultHost3D);

	/*
	 * Path to result file on remote machine
	*/
	char pathToResultFile[]= "/tmp/Team007/bmz_result.txt";

	/*
	 * Writes the results of BMZ calculation in file
	 */
	if(writeFile(pathToResultFile, resultHost3D)){
		printf("Write file finished!\n");
	}else{
		printf("Could not write file!\n");
	}

	/*
	 * Free allocated memory on device (GPU) and host
	 */
	cudaFree(listDevice);
	cudaFree(resultHost3D);
	cudaFree(listDevice);
	free(listHost);

	/*
	 * Destroys the threads which are currently operates
	 */
	cudaDeviceReset();

	return 0;
}
