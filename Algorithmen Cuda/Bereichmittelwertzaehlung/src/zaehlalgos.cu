/*
 * zaehlalgos.cu
 *
 *  Created on: Jan 15, 2018
 *  Author:
 *
 *  	This source file consists of "zaehlalgorithmen" and it's dependency algorithms, which
 *  	will be executed parallel with different data sets
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "datastructures.h"
#include "cuda_runtime.h"




__global__ void bmz(bmzlist **list, bmzresult** results, int n){
	int index = blockDim.x * blockIdx.x + threadIdx.x ;


	if(n > index){
		bmzlist* inputData = list[index];
			double maxTemp= inputData->hightemp;


		    int saved = 0;                              // Variable to check saved status of value
		    int tempJ = 0;                              // Variable for temp. j counter
		    double tempHigh = 0.0;                      // Variable for temp. highest value in one bereich
		    results[index]->hysterese = 0.02;                    // results[index]->hysterese in Percent
		    results[index]->classBorder = maxTemp/64;            // Size of results[index]->classBorders


		    // Definition of results[index]->kb]
		    results[index]->kb->size = ARRAY_SIZE;
		    for(int i = 0; i < results[index]->kb->size; i++){
		        results[index]->kb->bereich[i] = 0;
		        results[index]->kb->haufigkeit[i] = 0;
		    }
		    results[index]->kb->size = 0;

		    // Definition of results[index]->mw
		    results[index]->mw->size = ARRAY_SIZE;
		    for(int i = 0; i < results[index]->mw->size; i++){
		        results[index]->mw->bereich[i] = 0;
		        results[index]->mw->mittelwert[i] = 0;
		    }
		    results[index]->mw->size = 0;

		    // Iteration through input Data
		    for (int i = 0; i < inputData->size; i++ ){

		        tempHigh = 0.0;
		        saved = 0;

		        if(inputData->data[(i+1)] == 0.0){
		            break;
		        }

		        // If input[i] is smaller than input[i+1]
		        if(inputData->data[i] < inputData->data[(i+1)]){

		            // Iterate to next highest point of data
		            for(int j = i+1; inputData->data[i] < inputData->data[j]; j++){
		                if(tempHigh < inputData->data[j]){
		                    tempHigh = inputData->data[j];
		                    tempJ = j;
		                }else{
		                    break;
		                }
		            }
		            // Check for Rückstellbreite
		            if((tempHigh - inputData->data[i]) < (maxTemp * results[index]->hysterese)){
		                i = tempJ;
		                continue;
		            }

		            // Save Mittelwert Data
		            results[index]->mw->bereich[results[index]->mw->size] = (int) (((tempHigh - inputData->data[i])/results[index]->classBorder) + 0.5);
		            results[index]->mw->mittelwert[results[index]->mw->size] = (int) (((tempHigh/results[index]->classBorder + inputData->data[i]/results[index]->classBorder)/2) + 0.5);
		            results[index]->mw->size++;

		            // If data is already in array, then add one to haufigkeit
		            for(int x = 0; x < results[index]->kb->size; x++){
		                //if (results[index]->kb].bereich[x] == (tempHigh - (*inputData).data[i])){                             // NO results[index]->classBorderS
		                if (results[index]->kb->bereich[x] == (int) (((tempHigh - inputData->data[i])/results[index]->classBorder) + 0.5)){   // results[index]->classBorderS
		                    //results[index]->kb].mittelwert[x] = (int) (((tempHigh + (*inputData).data[i])/2) + 0.5);          // INTEGER MITTELWERT
		                    //results[index]->kb].mittelwert[x] = (((tempHigh + (*inputData).data[i])/2));                      // DOUBLE MITTELWERT
		                    results[index]->kb->haufigkeit[x]++;
		                    saved = 1;
		                    i = tempJ;
		                    break;
		                }
		            }

		            // If new value, then create new bereich & add one to haufigkeit
		            if (saved == 0){
		                results[index]->kb->size++;
		                for(int x = 0; x < results[index]->kb->size; x++){
		                    if (results[index]->kb->bereich[x] == 0){
		                        //results[index]->kb].bereich[x] = tempHigh - (*inputData).data[i];                             // NO results[index]->classBorderS
		                        results[index]->kb->bereich[x] = (int) (((tempHigh - inputData->data[i])/results[index]->classBorder) + 0.5); // results[index]->classBorderS
		                        //results[index]->kb].mittelwert[x] = (int) (((tempHigh + (*inputData).data[i])/2) + 0.5);      // INTEGER MITTELWERT
		                        //results[index]->kb].mittelwert[x] = (((tempHigh + (*inputData).data[i])/2));                  // DOUBLE MITTELWERT
		                        results[index]->kb->haufigkeit[x]++;
		                        saved = 1;
		                        i = tempJ;
		                        break;
		                    }
		                }
		            }

		        }

		    }
		    // BubbleSort of results[index]->kb] by bereich
		    int done = 0;
		    do{
		        done = 0;
		        for(int i = 0; i <= results[index]->kb->size - 2; i++){
		            if(results[index]->kb->bereich[i] > results[index]->kb->bereich[i+1]){
		                int tmpBereich = results[index]->kb->bereich[i];
		                int tmpHaufigkeit = results[index]->kb->haufigkeit[i];

		                results[index]->kb->bereich[i] = results[index]->kb->bereich[i+1];
		                results[index]->kb->haufigkeit[i] = results[index]->kb->haufigkeit[i+1];

		                results[index]->kb->bereich[i+1] = tmpBereich;
		                results[index]->kb->haufigkeit[i+1] = tmpHaufigkeit;
		                done = 1;
		            }
		        }
		    }while(done == 1);
	}



}

