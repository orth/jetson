/* main.cu
 *
 * Created on: Jan 15, 2018
 * Author Erlandas Miezys
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dataprep.h"
#include "zaehlalgos.h"
#include <sys/time.h>
#include "cuda_runtime.h"

#define CHECK_FOR_ERRORS(r) {_CHECK_FOR_ERRORS((r), __LINE__);}

// function for error checking while allocating unified memory
void _CHECK_FOR_ERRORS(cudaError_t r, int line){
	if(r != cudaSuccess){
		printf("CUDA error on line %d: %s\n", line, cudaGetErrorString(r));
		exit(0);
	}
}

// function for measuring execution time
float timedifference_msec(struct timeval t0, struct timeval t1){
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

// variable in unified memory space to make sure only required amount of threads on kernel will be executed
__device__ __managed__ int n;


int main(int args, char** argv){

	struct timeval t0, t1;
	float elapsed;

	const char* filepath = "/tmp/cudaZaehlalgos/data/testdata.csv";

	//******************bmz************************************************
	// defining pointer to an array
	bmzlist** list;
	// allocating memory for pointer to array
	CHECK_FOR_ERRORS(cudaMallocManaged(&list, sizeof(bmzlist*) * COLUMNSTOPROCESS));
	size_t i;
	// allocating memory for data structure in unified memory and assigning address
	for(i=0; i<COLUMNSTOPROCESS; i++){
		CHECK_FOR_ERRORS(cudaMallocManaged(&list[i], sizeof(bmzlist)));
		*list[i] = *getcolumndata(filepath, i+1);
	}


	// allocation of memory for the results in unified memory
	bmzresult** results;
	CHECK_FOR_ERRORS(cudaMallocManaged(&results, sizeof(bmzresult*) * COLUMNSTOPROCESS));
	for(i=0; i<COLUMNSTOPROCESS; i++){
		CHECK_FOR_ERRORS(cudaMallocManaged(&results[i], sizeof(bmzresult)));
		CHECK_FOR_ERRORS(cudaMallocManaged(&results[i]->kb, sizeof(klassenbreite)));
		CHECK_FOR_ERRORS(cudaMallocManaged(&results[i]->mw, sizeof(mittelwert)));
	}

	//********************************************************************


	// COLUMNSTOPROCESS defines the column number to be executed parallel on GPU
	// it can be changed in "datastructures" header file
	n = COLUMNSTOPROCESS;
	int blocksize = 1;
	dim3 gridsize(blocksize, 1, 1); 								// grid dimensions: x, y, z
	dim3 threadblocksize(ceil(COLUMNSTOPROCESS/blocksize), 1, 1);	// thread-block dimensions

	gettimeofday(&t0, NULL);

	// kernel call, which will be executed on GPU
	bmz<<<gridsize, threadblocksize>>>(list, results, n);


	cudaDeviceSynchronize();
	gettimeofday(&t1, NULL);
	elapsed = timedifference_msec(t0, t1);
	printf("Code on GPU executed in %f milliseconds.\n", elapsed);


	// prints results on the console
	//printbmzresults(results, COLUMNSTOPROCESS);

	//saves results to a txt file
	writebmzresultstofile(results, COLUMNSTOPROCESS);

	cudaFree(list);
	cudaFree(results);


	return 0;
	}
