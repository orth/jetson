/*
 * dataprep.h
 *
 *  Created on: Jan 14, 2018
 *  Author: Erlandas Miezys
 */

#ifndef DATAPREP_H_
#define DATAPREP_H_

#include "datastructures.h"

bmzlist* getcolumndata(const char*, int);
void printbmzresults(bmzresult**, int);
void writebmzresultstofile(bmzresult**, int);


#endif /* DATAPREP_H_ */
