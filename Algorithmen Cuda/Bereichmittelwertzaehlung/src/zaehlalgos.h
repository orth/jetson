/*
 * zaehlalgos.h
 *
 *  Created on: Jan 17, 2018
 *  Author: Erlandas Miezys
 */
#ifndef ZAEHLALGOS_H_
#define ZAEHLALGOS_H_

#include "datastructures.h"

__global__ void bmz(bmzlist**, bmzresult**, int);
__global__ void test();



#endif
