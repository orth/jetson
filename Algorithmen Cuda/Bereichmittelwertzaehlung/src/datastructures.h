/*
 *  datastructures.h
 *
 *  Created on: Jan 15, 2018
 *  Author: Erlandas Miezys
 *
 *      This header file defines all data structures used in the programm
 */


#ifndef DATASTRUCTURES_H_
#define DATASTRUCTURES_H_

#define ARRAY_SIZE 20000
#define N
#define COLUMNSTOPROCESS 8



// used for "bereichsmittelwertzaehlung" algorithm
typedef struct bmzlist{
    int size;
    double data[ARRAY_SIZE];
    int hightemp;
}bmzlist;

// used for "bereichsmittelwertzaehlung" algorithm
typedef struct klassenbreite{
    long size;
    int bereich[ARRAY_SIZE];
    int haufigkeit[ARRAY_SIZE];
    //int mittelwert[ARRAY_SIZE];
}klassenbreite;

// used for "bereichsmittelwertzaehlung" algorithm
typedef struct mittelwert{
    long size;
    int bereich[ARRAY_SIZE];
    int mittelwert[ARRAY_SIZE];
}mittelwert;

// used for "bereichsmittelwertzaehlung" algorithm

typedef struct bmzresult{
	klassenbreite* kb;
	mittelwert* mw;
	double hysterese;
	double classBorder;
}bmzresult;

#endif /* DATASTRUCTURES_H_ */
