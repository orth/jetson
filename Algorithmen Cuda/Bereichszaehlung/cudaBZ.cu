#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "datastructures.h"
#define SEKTORENZAHL 0.3


__global__ void bmz(bzlist **list, bmzresult** results){
/*Die funktion repräsentiert den Bereichszählung Algorithmus. Als übergabe bekommt die funktion eine Arrayliste von Double-Zahlen
und die Länge dieser Array. Berechnet werden die Anzahl der Zwischenbereichen für jeden Bereich und die Anzahl der Bereiche.
*/
	int blockindex = blockIdx.x ;
	bzlist* inputData = list[blockindex];
    //letzter datensatz
    double letzteZahl = 0;
    //vorletzter datensatz
    double vorletzteZahl = 0;

     results[blockindex]->bereiche = 0;
    int zwischenbereiche = 0;
	
	
	
	
	
    results[blockindex]->ergebnisse[results[blockindex]->bereiche]=zwischenbereiche;
	
	
    int tempInt = 0;
	
	
    double zbkontrolle = 0; //zwischenbereichskontrolle
    //Iterate through array list
    int x=0;
    for ( x; x<size; x++){
        if(list[blockindex]->data[x] < letzteZahl){
        //soll nichts machen wenn die werte absteigen
        }
        else{
            if(letzteZahl < vorletzteZahl){tempInt = 1;}
            else if(letzteZahl > vorletzteZahl){tempInt = 2;}
				else {tempInt = 0;}
				switch(tempInt)
				{
					case 0: //Wenn die letzte und die vorletzte Zahl gleich sind
						break;
					case 1: //Wenn die letzte Zahl kleiner als die vorletzte Zahl ist
						if(results[blockindex]->bereiche == 0){ //starten nach 1. extremwert

						}else{
							//printf("es gab %d zwischenbereiche im %d. Bereich\n",zwischenbereiche,bereiche);
						}

						results[blockindex]->bereiche += 1;
                        results[blockindex]->ergebnisse[results[blockindex]->bereiche]=1;
						//gibt automatisch ein zwischen bereich, wnen ein bereich anfängt
						zwischenbereiche = 1;
						zbkontrolle = list[blockindex]->data[x];

						break;

					case 2: //Wenn die letzte zahl großer als die vorletzte ist

						if(results[blockindex]->bereiche == 0){//starten nach 1. extremwert

						}else{

							if((zbkontrolle + SEKTORENZAHL) <= list[blockindex]->data[x]){  //+wert ändern für die bereichs sektoren

                                results[blockindex]->ergebnisse[results[blockindex]->bereiche]++;
								zwischenbereiche += 1;
								zbkontrolle = list[blockindex]data[x];
							}

						}

					   	break;

					default:
					   break;
				}
			}
			vorletzteZahl = letzteZahl;
			letzteZahl = list[blockindex]->data[x];

		


    }}
	// saves results to a txt file
void writebmzresultstofile(bmzresult** results, int columnstoprocess){

	FILE* file = fopen("/home/nvidia/projects/chris/Bereichszaehlung/results.txt", "w");

	int i;
	for(i=0; i<columnstoprocess; i++){
		fprintf(file,"Bereichszaehlung \n");
		fprintf(file,"COLUMN: %d\n", i+1);
		
		
		fprintf(file,"############################## - Start \n");
		
			
		for(int z=1;z<=list->bereiche;z++){
                fprintf(file,"es gab %d zwischenbereiche im %d. Bereich\n\n",results->ergebnisse[z],z);
                
		}
			
		fprintf(file,"############################## - Ende \n\n");

		


	}
	fclose(file);
}