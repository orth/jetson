/*
 * zaehlalgos.h
 *
 *  Created on: Jan 17, 2018
 *  Author: Erlandas Miezys
 */
#ifndef BZ_H_
#define BZ_H_

#include "datastructures.h"

__global__ void bmz(bmzlist**, bmzresult**);
void printbmzresults(bmzresult**, int);
void writebmzresultstofile(bmzresult**, int);


#endif