/*
 * dataprep.cu
 *
 *  Created on: Jan 15, 2018
 *  Author:
 *
 *  Source file with data preparation algorithms
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "datastructures.h"


#define LINE_SIZE 1024
#define STARTING_POINT 8
#define MAX_SPEED 100
#define ARRAY_SIZE 20000



char * replace_char(char * input, char find, char replace) {
    char* output = (char*)malloc(strlen(input));

    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == find) output[i] = replace;
        else output[i] = input[i];
    }

    output[strlen(input)] = '\0';

    return output;
}

char* getFieldNumber(char* line, int num){
    char* temp;

    for (temp = strtok(line, ";"); temp && *temp; temp = strtok(NULL, ";\n")){
        if (!--num){
            if(isdigit(*temp)){
                temp = replace_char(temp, ',', '.');
                return temp;
            }
		}
        }
    return "X";
}

// Returns the desired column of the .csv file, it also filters the zero values out
bmzlist* getcolumndata(char* filepath, int column){
	bmzlist* data = (bmzlist*)malloc(sizeof(bmzlist));
	data->size = 0;

	for(int i = 0; i < data->size; i++){
        data->data[i] = 0;
    }

	FILE* stream = fopen(filepath, "r");
	if(stream == NULL){
		printf("Error reading file\n");
		}
	char line[LINE_SIZE];
	int start = 0;
	int count = 0;
	double hightemp = 0.0;

	while (fgets(line, LINE_SIZE, stream)){
        if(start != STARTING_POINT){
            start++;
        }else{
            char *endptr;
            char* tmp = strdup(line);
            char* returnTmp = getFieldNumber(tmp, column);
            if(strcmp(returnTmp, "X") != 0){
                // STRING TO DOUBLE CONVERSION
                double valueTmp = strtod(returnTmp, &endptr);
                 //CHECK IF valueTmp IS ZERO
                if(!(valueTmp == 0.0)){
                    if(valueTmp > hightemp && valueTmp < MAX_SPEED){
                        // SET HIGHEST DATA VALUE
                        hightemp = valueTmp;
                    }
                    if(valueTmp < MAX_SPEED){
                        data->size++;
                        data->data[count] = valueTmp;
                        count++;
                    }

                }
             }
            free(tmp);
        }
	}
	data->hightemp = hightemp;
	return data;
}

