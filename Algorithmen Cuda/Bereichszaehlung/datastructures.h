/*
 *  datastructures.h
 *
 *  Created on: Jan 15, 2018
 *  Author: Erlandas Miezys
 *
 *      This header file defines all data structures used in the programm
 */

#ifndef DATASTRUCTURES_H_
#define DATASTRUCTURES_H_

#define ARRAY_SIZE 20000
#define ERGEBNISSE 1000


typedef struct bmzlist{
	int size;
	double data[ARRAY_SIZE];
    int hightemp;
}bmzlist;



typedef struct bmzresult{
	int bereiche;
	int ergebnisse[ERGEBNISSE];
}bmzresult;

#endif /* DATASTRUCTURES_H_ */
