/*
 * dataprep.h
 *
 *  Created on: Jan 14, 2018
 *  Author: Erlandas Miezys
 */

#ifndef DATAPREP_H_
#define DATAPREP_H_

#include "datastructures.h"

bmzlist* getcolumndata(char*, int);


#endif /* DATAPREP_H_ */
