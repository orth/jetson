/* main.cu
 *
 * Created on: Jan 15, 2018
 * Author Erlandas Miezys
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dataprep.h"
#include "bz.h"
#include <sys/time.h>

float timedifference_msec(struct timeval t0, struct timeval t1){
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int main(int args, char** argv){

	struct timeval t0, t1;
	float elapsed;
	int columnstoprocess = 6;

	char* filepath = "/home/nvidia/projects/chris/Bereichszaehlung/testdata.csv";


	// defining pointer to array
	bmzlist** list;
	// allocating memory for pointer to array
	cudaMallocManaged(&list, sizeof(bmzlist*) * columnstoprocess);
	int i;
	// allocating memory for structure in unified memory and assigning address
	for(i=0; i<columnstoprocess; i++){
		cudaMallocManaged(&list[i], sizeof(bmzlist));
		*list[i] = *getcolumndata(filepath, i+1);
	}



	// allocation of memory for the results in unified memory
	bmzresult** results;
	cudaMallocManaged(&results, sizeof(bmzresult*) * columnstoprocess);
	for(i=0; i<columnstoprocess; i++){
		cudaMallocManaged(&results[i], sizeof(bmzresult));

	}


	int blocks = columnstoprocess;
	int threads = 1;
	gettimeofday(&t0, NULL);
	bmz<<<blocks,threads>>>(list, results);
	cudaDeviceSynchronize();
	gettimeofday(&t1, NULL);
	elapsed = timedifference_msec(t0, t1);
	printf("Code on GPU executed in %f milliseconds.\n", elapsed);
	//printbmzresults(results);
	//saves results to a txt file
	writebmzresultstofile(results, columnstoprocess);

	return 0;
	}
