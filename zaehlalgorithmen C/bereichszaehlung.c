#include <stdio.h>
#include <stdlib.h>
#define DATASIZE 1000000
#define ERGEBNISSE 1000

typedef struct List {
    /*Hier werden die double Zahlen gespeichert und die Laenge der Array*/
	int size;
	double data[DATASIZE];
	int ergebnisse[ERGEBNISSE];
	int bereiche;
}List;


void bz(double *,int,double,List *);
char* getfield(char*, int);
double convertStringToDouble(char *);

int main()
{
    int spalte;
    printf("Geben Sie Spalte ein.\n");
    scanf("%d", &spalte);
    double sektorenZahl;
    printf("Geben Sie eine double Zahl fuer die Berechnunng der Zwischenbereiche.\n");
    scanf("%lf", &sektorenZahl);

    List *list = malloc(sizeof *list);

	double data;
	char dataString[1024];
	char *tmp;
	FILE* stream = fopen("Testdaten2.csv", "r");
	int i=0;
	while(fgets(dataString, 1024, stream) != NULL) {
        tmp=getfield(dataString,spalte);
		list->data[i] = convertStringToDouble(tmp);
		i++;
	}
    list->size = i+1;

    //Bereichzaehlung

    bz(list->data,list->size,sektorenZahl,list);



    return 0;

}
double convertStringToDouble(char * str) {
    /*Die Funktion nimmt eine Zeichenkette, die eine Double-Zahl representiert und ersetzt das Komma(,) mit einem Punkt(.).
Am ende mit hilfe der function Atof wird die Zeichenkette in double umgewandelt und zurückgegeben*/
	int length = strlen(str);
	int i;

	for(i=0;i<length;i++) {
		if(str[i] == ',') {
			str[i] = '.';
		}
	}

	return atof(str);
}
char* getfield(char* line, int spalte)
/*Die funktion bekommt bei jedem Aufruf jeweils eine ganze Zeile aus dem CSV Datei und gibt züruck als Zeichenkette die gewünschte stelle.
Somit kann man eine Gewünschte spalte aus dem Datei auswählen  */
{
	char* tok;
	for (tok = strtok(line, ";");
		tok && *tok;
		tok = strtok(NULL, ";\n"))
		{
			if (!--spalte)
				return tok;
		}
	return NULL;
}
void bz(double *data,int size,double sektorenZahl,List *list){
/*Die funktion repräsentiert den Bereichszählung Algorithmus. Als übergabe bekommt die funktion eine Arrayliste von Double-Zahlen
und die Länge dieser Array. Berechnet werden die Anzahl der Zwischenbereichen für jeden Bereich und die Anzahl der Bereiche.
*/

    //letzter datensatz
    double letzteZahl = 0;
    //vorletzter datensatz
    double vorletzteZahl = 0;

     list->bereiche = 0;
    int zwischenbereiche = 0;
    list->ergebnisse[list->bereiche]=zwischenbereiche;
    int tempInt = 0;
    double zbkontrolle = 0; //zwischenbereichskontrolle
    //Iterate through array list
    int x=0;
    for ( x; x<size; x++){
        if(data[x] < letzteZahl){
        //soll nichts machen wenn die werte absteigen
        }
        else{
            if(letzteZahl < vorletzteZahl){tempInt = 1;}
            else if(letzteZahl > vorletzteZahl){tempInt = 2;}
				else {tempInt = 0;}
				switch(tempInt)
				{
					case 0: //Wenn die letzte und die vorletzte Zahl gleich sind
						break;
					case 1: //Wenn die letzte Zahl kleiner als die vorletzte Zahl ist
						if(list->bereiche == 0){ //starten nach 1. extremwert

						}else{
							//printf("es gab %d zwischenbereiche im %d. Bereich\n",zwischenbereiche,bereiche);
						}

						list->bereiche += 1;
                        list->ergebnisse[list->bereiche]=1;
						//gibt automatisch ein zwischen bereich, wnen ein bereich anfängt
						zwischenbereiche = 1;
						zbkontrolle = data[x];

						break;

					case 2: //Wenn die letzte zahl großer als die vorletzte ist

						if(list->bereiche == 0){//starten nach 1. extremwert

						}else{

							if((zbkontrolle + sektorenZahl) <= data[x]){  //+wert ändern für die bereichs sektoren

                                list->ergebnisse[list->bereiche]++;
								zwischenbereiche += 1;
								zbkontrolle = data[x];
							}

						}

					   	break;

					default:
					   break;
				}
			}
			vorletzteZahl = letzteZahl;
			letzteZahl = data[x];

			//wenn der letzter datensatz kommt
           if(x == size - 1){

        int z;
		for(z=1;z<=list->bereiche;z++){
                printf("es gab %d zwischenbereiche im %d. Bereich\n",list->ergebnisse[z],z);
                printf("ERGEBNISS der Bereichzaehlung:\n\nEs gibt %d bereiche\n",z);
		}
            }


    }}

