#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct List {
	int size;
	double data[1000000];
}List;

/*
prints the result of fieldavarage-algorithm 
*/

void printAverageCount(struct List *list) {
	double tempDouble = 0;  //letzter datensatz
	double tempDouble2 = 0;  //vorletzter datensatz
		
		int tempInt = 0; //switch case controller
		double total = 0; //sum of bereich
		int totalDivisor = 0; //total of number in bereich
		int len = list->size;
		int x;
		
		for (x=0; x<len; x++){ //Iterate through array list
			if(len < tempDouble ){  //do nothing if outside of bereich
				
			}else{
				if(tempDouble < tempDouble2){tempInt = 1;}  //new bereich starts because ascending started on last two values
				else if(tempDouble > tempDouble2){tempInt = 2;} //inside a bereich
				else if(tempDouble == tempDouble2){tempInt = 3;} //inside a bereich, 2 values same
				else {tempInt = 0;} //reset to 0 if none fit
				switch(tempInt)
				{
					case 0:
						break;
					case 1:
						printf("Field Average: %d\n", (total/totalDivisor));
						total = list->data[x];
						totalDivisor = 1;
						break; 
					   
					case 2: 
						total +=list->data[x];
						totalDivisor++;
					   	break;
					   	
					case 3:
						total += list->data[x];
						totalDivisor++;
					   	break;
					   	
					default:
					   break;
				}
			}
			tempDouble2 = tempDouble; //triangle switch
			tempDouble = list->data[x];; //triangle switch
			
			if(len - 1){ //end of data
				printf("Done\n");
			}
		}
}

/*
	converts a given string to a double
*/
double convertStringToDouble(char * str) {
	int length = strlen(str);
	int i;

	for(i=0;i<length;i++) {
		if(str[i] == ',') {
			str[i] = '.';
		}
	}

	return atof(str);
}

int main(int argc, char **argv) {
	
	List *list = malloc(sizeof *list);
	FILE *CSV;
	double data;
	char dataString[1024];

	if(argc < 2) {
      		fprintf(stderr, "Verwendung : %s datei.csv\n", *argv);
      		return EXIT_FAILURE;
   	}

   	CSV = fopen(argv[1], "r");
   	if(NULL == CSV) {
      		fprintf(stderr, "Fehler beim Oeffnen ...\n");
      		return EXIT_FAILURE;
   	}
	
	int i=0;
	while(fgets(dataString, 1024, CSV) != NULL) {
		list->data[i] = convertStringToDouble(dataString);

		i++;
	}
	
	list->size = i+1;
	printAverageCount(list);


   return EXIT_SUCCESS;
}