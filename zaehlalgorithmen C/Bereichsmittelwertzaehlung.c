//
//  BMZ.c
//  Bereichs-Mittelwert-Zählung
//
//  Created by Onur Erdem on 13.11.17.


#include <stdio.h>
#include <stdlib.h>

// PARAMETERS
#define FILE_PATH "/Users/o2to/Desktop/Mittelwertzählung/Mittelwertzählung/input.csv"         // PATH OF CSV FILE
#define ARRAY_SIZE 20000                                                                      // SIZE OF ARRAY FOR DATA
#define LINE_SIZE 1024                                                                        // MAXIMAL LENGTH OF ONE ROW IN CSV FILE
#define STARTING_POINT 8                                                                      // STARTING ROW OF DATA IN CSV FILE
#define COLLUMN 3                                                                             // WANTED COLLUMN FOR DATA SET
#define MAX_SPEED 100                                                                         // MAXIMAL SPEED
#define HYSTERESE 0.005                                                                        // Hysterese in percent
#define CLASSES 64                                                                           // COUNT OF CLASSES

struct list{
    int size;
    double data[ARRAY_SIZE];
};

struct klassenbreite{
    long size;
    int bereich[ARRAY_SIZE];
    int haufigkeit[ARRAY_SIZE];
    //int mittelwert[ARRAY_SIZE];
};

struct mittelwert{
    long size;
    int bereich[ARRAY_SIZE];
    int mittelwert[ARRAY_SIZE];
};


// REPLACE UNWANTED CHARS
char * replace_char(char * input, char find, char replace) {
    char* output = (char*)malloc(strlen(input));
    
    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == find) output[i] = replace;
        else output[i] = input[i];
    }
    
    output[strlen(input)] = '\0';
    
    return output;
}

// GET WANTED COLLUMN FROM DATA SET & RETURN AS STRING
const char* getFieldNumber(char* line, int num){
    const char* temp;
    for (temp = strtok(line, ";"); temp && *temp; temp = strtok(NULL, ";\n")){
        if (!--num){
            if(isnumber(*temp)){
                temp = replace_char(temp, ',', '.');
                return temp;
            }
        }
        }
    return "X";
}


//BEREICHS-MITTELWERT-ZÄHLUNG
void bmz(struct list *inputData, double maxTemp){
    
    struct klassenbreite bmzData;               // New Data Set for Data after BMZ algorithm
    struct mittelwert mwData;                   // New Data Set for mittelwert data
    int saved = 0;                              // Variable to check saved status of value
    int tempJ = 0;                              // Variable for temp. j counter
    double tempHigh = 0.0;                      // Variable for temp. highest value in one bereich
    double hysterese = HYSTERESE;               // Hysterese in Percent
    double classBorder = maxTemp/CLASSES;       // Size of Classborders
    
    // Definition of bmzData
    bmzData.size = ARRAY_SIZE;
    for(int i = 0; i < bmzData.size; i++){
        bmzData.bereich[i] = 0;
        bmzData.haufigkeit[i] = 0;
    }
    bmzData.size = 0;
    
    // Definition of mwData
    mwData.size = ARRAY_SIZE;
    for(int i = 0; i < mwData.size; i++){
        mwData.bereich[i] = 0;
        mwData.mittelwert[i] = 0;
    }
    mwData.size = 0;
    
    // Iteration through input Data
    for (int i = 0; i < (*inputData).size; i++ ){
        
        tempHigh = 0.0;
        saved = 0;
        
        if((*inputData).data[(i+1)] == 0.0){
            break;
        }
        
        // If input[i] is smaller than input[i+1]
        if((*inputData).data[i] < (*inputData).data[(i+1)]){
            
            // Iterate to next highest point of data
            for(int j = i+1; (*inputData).data[i] < (*inputData).data[j]; j++){
                if(tempHigh < (*inputData).data[j]){
                    tempHigh = (*inputData).data[j];
                    tempJ = j;
                }else{
                    break;
                }
            }
            // Check for Rückstellbreite
            if((tempHigh - (*inputData).data[i]) < (maxTemp * hysterese)){
                i = tempJ;
                continue;
            }
            
            // Save Mittelwert Data
            mwData.bereich[mwData.size] = (int) (((tempHigh - (*inputData).data[i])/classBorder) + 0.5);
            mwData.mittelwert[mwData.size] = (int) (((tempHigh/classBorder + (*inputData).data[i]/classBorder)/2) + 0.5);
            mwData.size++;
            
            // If data is already in array, then add one to haufigkeit
            for(int x = 0; x < bmzData.size; x++){
                //if (bmzData.bereich[x] == (tempHigh - (*inputData).data[i])){                             // NO CLASSBORDERS
                if (bmzData.bereich[x] == (int) (((tempHigh - (*inputData).data[i])/classBorder) + 0.5)){   // CLASSBORDERS
                    //bmzData.mittelwert[x] = (int) (((tempHigh + (*inputData).data[i])/2) + 0.5);          // INTEGER MITTELWERT
                    //bmzData.mittelwert[x] = (((tempHigh + (*inputData).data[i])/2));                      // DOUBLE MITTELWERT
                    bmzData.haufigkeit[x]++;
                    saved = 1;
                    i = tempJ;
                    break;
                }
            }
            
            // If new value, then create new bereich & add one to haufigkeit
            if (saved == 0){
                bmzData.size++;
                for(int x = 0; x < bmzData.size; x++){
                    if (bmzData.bereich[x] == 0){
                        //bmzData.bereich[x] = tempHigh - (*inputData).data[i];                             // NO CLASSBORDERS
                        bmzData.bereich[x] = (int) (((tempHigh - (*inputData).data[i])/classBorder) + 0.5); // CLASSBORDERS
                        //bmzData.mittelwert[x] = (int) (((tempHigh + (*inputData).data[i])/2) + 0.5);      // INTEGER MITTELWERT
                        //bmzData.mittelwert[x] = (((tempHigh + (*inputData).data[i])/2));                  // DOUBLE MITTELWERT
                        bmzData.haufigkeit[x]++;
                        saved = 1;
                        i = tempJ;
                        break;
                    }
                }
            }
            
        }
        
    }
    
    // BubbleSort of bmzData by bereich
    int done = 0;
    do{
        done = 0;
        for(int i = 0; i <= bmzData.size - 2; i++){
            if(bmzData.bereich[i] > bmzData.bereich[i+1]){
                int tmpBereich = bmzData.bereich[i];
                int tmpHaufigkeit = bmzData.haufigkeit[i];
                
                bmzData.bereich[i] = bmzData.bereich[i+1];
                bmzData.haufigkeit[i] = bmzData.haufigkeit[i+1];
                
                bmzData.bereich[i+1] = tmpBereich;
                bmzData.haufigkeit[i+1] = tmpHaufigkeit;
                done = 1;
            }
        }
    }while(done == 1);
 
    // Console Print of bmzData
    printf("Bereichsklassenzaehlung \n");
    printf("############################## - Start \n");
    for(int i = 0; i < bmzData.size; i++){
        if(bmzData.bereich[i] != 0){
        printf("Bereich: %d    ", bmzData.bereich[i]);
        printf("\tHäufigkeit: %d    ", bmzData.haufigkeit[i]);
        //printf("\tMittelwert: %d    ", bmzData.mittelwert[i]);
        printf("\n");
        }
    }
    printf("############################## - Ende \n\n");
    
    // Console Print of mwData
    printf("Mittelwert Bestimmung \n");
    printf("############################################ - Start\n");
    for(int i = 0; i < mwData.size; i++){
        if(mwData.bereich[i] != 0){
            printf("Bereichsklasse: %d    ", mwData.bereich[i]);
            printf("\tMittelwertklasse: %d    ", mwData.mittelwert[i]);
            printf("\n");
        }
    }
    
    printf("############################################ - Ende \n\n");
    
    // Console Print etc
    printf("\nEingestellte Hysterese: \t\t%.3f%%       ", hysterese);
    printf("\nErrechnete Hysterese: \t\t\t%.2f       ", maxTemp * hysterese);
    printf("\nMaximale Geschwindigkeit: \t\t%.2f       ", maxTemp);
    printf("\nAnzahl der Klassen: \t\t\t%d       ", CLASSES);
    printf("\nErrechnete Klassengroesse: \t\t%f       ", classBorder);
    printf("\n\n");
}

// MAIN METHOD
int main(int argc, const char * argv[]) {
    
    struct list data;
    data.size = 0;
    
    // Reset of data
    for(int i = 0; i < data.size; i++){
        data.data[i] = 0;
    }
    
    FILE* stream = fopen(FILE_PATH, "r");
    char line[LINE_SIZE];
    int start = 0;
    int count = 0;
    double highTemp = 0.0;
    
    while (fgets(line, LINE_SIZE, stream))
    {
        if(start != STARTING_POINT){
            start++;
        }else{
            char *endptr;
            char* tmp = strdup(line);
            char* returnTmp = getFieldNumber(tmp, COLLUMN);
            if(strcmp(returnTmp, "X") != 0){
                // STRING TO DOUBLE CONVERSION
                double valueTmp = strtod(returnTmp, &endptr);
                 //CHECK IF valueTmp IS ZERO
                if(!(valueTmp == 0.0)){
                    if(valueTmp > highTemp && valueTmp < MAX_SPEED){
                        // SET HIGHEST DATA VALUE
                        highTemp = valueTmp;
                    }
                    if(valueTmp < MAX_SPEED){
                        data.size++;
                        data.data[count] = valueTmp;
                        count++;
                        
                        //printf("DATA: %f \n", valueTmp);
                    }
                    
                }
            }
            
            free(tmp);
        }
        
    }
    
    // START BMZ
    bmz(&data, highTemp);
    //printf("MAX KMH: %f \n", highTemp);
    
    
    return 115;
}
