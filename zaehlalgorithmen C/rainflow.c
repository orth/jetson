#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "csvHandler.h"
//#include "listHelper.h"

#define KLASSEN 32


int numberOfClasses = KLASSEN;
int classCounter[KLASSEN];
int mittelSdZ[KLASSEN][KLASSEN];
double minValue = 0.0;
double maxValue = 0.0;
double classSize = 0.0;

// Structure to handle the Sensor Data
struct t_node {
	double number;
	struct t_node *next;
};

char* getfield(char* line, int num)
{
    char* tok;
    for (tok = strtok(line, ";");
            tok && *tok;
            tok = strtok(NULL, ";\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}

// Delets the Next Node after the one given too the function
void deleteNextNode(struct t_node *head) {
	struct t_node * getDel_node = malloc(sizeof(struct t_node));
	getDel_node = head->next;
	head->next = head->next->next;
	free(getDel_node);
	getDel_node = NULL;
}

// Add Data to list
void insert (struct t_node *head, double num){
	struct t_node * new_node = malloc(sizeof(struct t_node));
	new_node->number = num;
	
	struct t_node * temp_node = malloc(sizeof(struct t_node));
	temp_node = head;
	
	while (temp_node->next != NULL) {
		temp_node = temp_node->next;
	}
	
	temp_node->next = new_node;
	new_node->next = NULL;
}


void setClassCrossing (double top, double bottom){
	printf("Top:%lf, Bottom:%lf\n", top, bottom);

	double dif = top - bottom;
	classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;
	
	printf("Number of Classes: %d\n", numberOfClasses);
	// For mittelwert and 'Schwingbreite des Zyklus'
	double mittelwert = top - (dif/2);
	double sdz = dif;
	
	printf("Mittel:%lf, sdz:%d\n", mittelwert, (int)(sdz/classSize));
	printf("MittelW:%d, sdz:%d\n", (int)(mittelwert/classSize), (int)(sdz/classSize));

	int mittelwertArr = (int)(mittelwert/classSize);
	if (mittelwertArr < 0){
		mittelwertArr = 0 - mittelwertArr;
	}

	int sdzArr = (int)(sdz/classSize);
	if (sdzArr < 0){
		sdzArr = 0 - sdzArr;
	}

	mittelSdZ[mittelwertArr][sdzArr] = mittelSdZ[mittelwertArr][sdzArr] + 1;

	//printf("3ClassCross%d\n", numberOfClasses);
}

void printMittelSdZ(void) {
	int i = 0, j = 0;
	printf("\n\nZählergebnis Rainflow\n\n");
	printf("Schwingbreite des Zyklus\n");
	//printf("Schwingbreite des Zyklus\n\x18\x19\x1a\x1b\n");
	
	while (i < numberOfClasses) {
		
		printf("%d\t|", i);
		while (j < numberOfClasses) {
			if (mittelSdZ[j][i] != 0) {
				printf("%d\t", mittelSdZ[j][i]);
			} else {
				printf("-\t");
			}
			j++;
		}
		j = 0;
		printf("\n");
		i++;
	}

	printf("        ");	
	while (j < numberOfClasses) {
		printf("--------");		
		j++;
	}
	j = 0;
	printf("\n");

	printf("\t");		
	while (j < numberOfClasses) {
		printf(" %d\t", j+1);		
		j++;
	}
	j = 0;
	printf("\n");
	printf("\tMittelwert");
	printf("\n\n");
}


void printlistvalues(struct t_node *head){
	printf("All List Values\n");
	struct t_node * temp_node = malloc(sizeof(struct t_node));
	temp_node = head;
	while (temp_node != NULL){
		printf("%lf\n", temp_node->number);
		temp_node = temp_node->next;
	}
}

// Delete values which are not a valley or a hill
void thinOutValues(struct t_node *head) {
	struct t_node * temp_node = malloc(sizeof(struct t_node));
	temp_node = head;
	
	while (temp_node->next->next != NULL) {
		if (temp_node->number <= temp_node->next->number && temp_node->next->number <= temp_node->next->next->number) {
			deleteNextNode(temp_node);
		} else if (temp_node->number >= temp_node->next->number && temp_node->next->number >= temp_node->next->next->number) {
			deleteNextNode(temp_node);
		} else {
			temp_node = temp_node->next;
		}
	}
}

// Search the list for the minimum and the maximum value
void getMinMaxValues (struct t_node *head) {
	struct t_node * temp_node = malloc(sizeof(struct t_node));
	temp_node = head;
	minValue = head->number;
	maxValue = head->number;
	
	while (temp_node != NULL) {
		if (temp_node->number < minValue) {
			minValue = temp_node->number;
		}
		if (temp_node->number > maxValue) {
			maxValue = temp_node->number;
		}
		temp_node = temp_node->next;
	}
	classSize = (maxValue - minValue) / (double)numberOfClasses;
}

void levelAtNull(struct t_node *head) {
	struct t_node * temp_node = malloc(sizeof(struct t_node));
	temp_node = head;
		
	while (temp_node != NULL) {
		temp_node->number = temp_node->number - minValue;
		temp_node = temp_node->next;
	}
	getMinMaxValues(head);
}


// Count hysteresis
void countHistery(struct t_node *head) {
	
	while (1 == 1) {
	if (head->next == NULL) {
		break;
	} else if (head->next->next == NULL) {
		break;
	} else if (head->next->next->next == NULL) {
		break;
	} else if (head->next->next->next->next == NULL) {
		break;
	}
	/*
	// For Debugging to view next Numbers to be worked with
	printf("%lf\n", head->number);
	printf("%lf\n", head->next->number);
	printf("%lf\n", head->next->next->number);
	printf("%lf\n", head->next->next->next->number);
	printf("%lf\n\n", head->next->next->next->next->number);
	displayHisteryCounter();
	*/
		if (head->number > head->next->number) {
			if (head->next->next->number >= head->number) {
				setClassCrossing(head->number, head->next->number);
				/*double dif = head->number - head->next->number;
				//printf("\nDIF %lf\n", dif);
				classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
				deleteNextNode(head);
			} else {
				if (head->next->next->next->number >= head->next->number) {
					setClassCrossing(head->next->next->number, head->next->next->next->number);
					/*double dif = head->next->next->number - head->next->next->next->number;
					//printf("\nDIF %lf\n", dif);
					classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
					deleteNextNode(head->next);
					deleteNextNode(head->next);
				} else {
					setClassCrossing(head->next->next->number, head->next->number);
					/*double dif = head->next->next->number - head->next->number;
					//printf("\nDIF %lf\n", dif);
					classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
					deleteNextNode(head);
					deleteNextNode(head);
				}
			}
		} else if (head->number < head->next->number) {
			if (head->number >= head->next->next->number) {
				setClassCrossing(head->next->number, head->number);
				/*double dif = head->next->number - head->number;
				classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
				deleteNextNode(head);
			} else {
				if (head->next->next->next->number <= head->next->number) {
					setClassCrossing(head->next->next->next->number, head->next->next->number);
					/*double dif = head->next->next->next->number - head->next->next->number;
					classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
					deleteNextNode(head->next);
					deleteNextNode(head->next);
				} else {
					setClassCrossing(head->next->number, head->next->next->number);
					/*double dif = head->next->number - head->next->next->number;
					classCounter[(int)(dif/classSize)] = classCounter[(int)(dif/classSize)]+2;*/
					deleteNextNode(head);
					deleteNextNode(head);
				}
			}
		} else {
			thinOutValues(head);
		}
	}
	printlistvalues(head);
}


void displayHisteryCounter (void) {
	int i = 1;
	while (i <= numberOfClasses) {
		printf("%d.\t%d\n", i, classCounter[i-1]);
		i++;
	}
}


// Copied from Stackoverflow
char *str_replace(char *search , char *replace , char *subject)
{
    char  *p = NULL , *old = NULL , *new_subject = NULL ;
    int c = 0 , search_size;
     
    search_size = strlen(search);
     
    //Count how many occurences
    for(p = strstr(subject , search) ; p != NULL ; p = strstr(p + search_size , search))
    {
        c++;
    }
     
    //Final size
    c = ( strlen(replace) - search_size )*c + strlen(subject);
     
    //New subject with new size
    new_subject = malloc( c );
     
    //Set it to blank
    strcpy(new_subject , "");
     
    //The start position
    old = subject;
     
    for(p = strstr(subject , search) ; p != NULL ; p = strstr(p + search_size , search))
    {
        //move ahead and copy some text from original subject , from a certain position
        strncpy(new_subject + strlen(new_subject) , old , p - old);
         
        //move ahead and copy the replacement text
        strcpy(new_subject + strlen(new_subject) , replace);
         
        //The new start position after this search match
        old = p + search_size;
    }
     
    //Copy the part after the last search match
    strcpy(new_subject + strlen(new_subject) , old);
     
    return new_subject;
}

int main(int argc, char* argv[]){
 	if (argc < 3) {
		printf("Verwendung: ./a.out <CSV-Datei> <Spaltennummer>\n");
		return 0;
	}
	struct t_node *list = malloc(sizeof(struct t_node));
	
		printf("foo");

	//int welcherLauf = 1;
	int welcheVariable = atoi(argv[2]);
	
	// runTestD(list);

	FILE* stream = fopen(argv[1], "r");

	char line[4096];
	while (fgets(line, 4096, stream)) {
		char* tmp = strdup(line);
		char* tump = strdup(line);
		printf("tmp Value would be %s\n",tump);
		char* rat = getfield(tump, welcheVariable);
		printf("Value would be %s\n", str_replace(",", ".", rat));
		double dreck = atof(str_replace(",", ".", rat));
		printf("Value would be %lf\n\n",dreck);
		insert(list, dreck);
		// NOTE strtok clobbers tmp
		free(tmp);
	}

	//To check if values are imported alright
	//printlistvalues(list);
	//printf("\n\n\n\n");

	list = list->next;

	thinOutValues(list);

	getMinMaxValues(list);
	levelAtNull(list);

	printf("\n\nMinimum:\t%lf", minValue);
	printf("\nMaximum:\t%lf\n", maxValue);
	printf("\nKlassengroesse:\t%lf\n\n", classSize);

	countHistery(list);

	displayHisteryCounter();

	printMittelSdZ();
	return 0;
}

