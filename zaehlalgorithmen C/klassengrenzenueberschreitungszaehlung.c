#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct List {
	int size;
	double data[1000000];
}List;

/*
	returns 1, if direction is up, 0 if not;
*/
int isDirectionUp(int prev, int next) {
	if(next > prev) {
		return 1;
	}
	else {
		return 0;
	}
}

/*
	 returns 1,  if data is int the current classRange, 0 if not 	
*/
int isInClassRange(int prev, int next,int lowerBounds, int upperBounds) {


	if(prev >= lowerBounds && next < upperBounds) {
		return 1;
	}
	else {
		return 0;
	}
}

/*
	return the maximum value in the gieven list
*/
float getMaxValue(struct List *list) {
	int i;
	float maxValue=0.0;
	for(i=0;i<list->size;i++) {
		if(list->data[i] > maxValue) {
			maxValue = list->data[i];
		}
	}

	return maxValue;
}

/*
	return the minimumvalue in the gieven list
*/
float getMinValue(struct List *list) {
	int i;
	float minValue = getMaxValue(list);
	for(i=0;i<list->size;i++) {
		if(list->data[i] < minValue) {
			minValue = list->data[i];
		}
	}
	return minValue;
}

/*
	returns the number of classes
*/
int getAmountOfClasses(float maxValue, int size) {

	int classRange = 0;

	
	if((maxValue / size) < 1) {
		classRange = 1;
	}
	else {
		classRange = (int) maxValue / size;
	}
	return classRange;
}

/*

	returns the classes filled with bounds
*/
int* getClasses(struct List *list) {
	float maxValue = getMaxValue(list);
	int size = list->size;
	
	int i,currentValue = 0;
	int amountOfClasses = getAmountOfClasses(maxValue, size);
	int * classes = malloc(amountOfClasses);
	classes[0] = 0;
	
	for(i=1;i<amountOfClasses;i++) {
		currentValue = (int) (currentValue + amountOfClasses);
		classes[i] = currentValue;
	}
	return classes;
}


/*
	dump all classes
*/
void printClasses(struct List * list) {
	int * classes = getClasses(list),i;
	for(i=0;i<list->size;i++) {
		printf("classes[%d] = %d\n",i,*classes);
		classes++;
	}
}


/*
	calculate the algorithm and prints out the crossed classes for a data set
*/
int * getCrossedClasses(int oldValue, int newValue, int * classes, int numberOfClasses) {
	int class_no;

	int count = 0;
	int * crossedClasses = malloc(numberOfClasses);
		
	if(isDirectionUp(oldValue, newValue) == 1) {
		for(class_no=0;class_no < numberOfClasses;class_no++) {
			int lowerBound = *(classes+class_no);
			int upperBound = *(classes+class_no+1);	
			int inClassRange = isInClassRange(oldValue, newValue, lowerBound, upperBound);

			if(oldValue < lowerBound && newValue < upperBound ==1) {
				break;
			}

			if(inClassRange == 0) {
				int crossedClass = class_no +1;
				*(crossedClasses+class_no) = crossedClass;
				printf("CrossedClass = %d\n", crossedClass);
			}
		
		}
	}
	return crossedClasses;
}


/*
	printf the result of the algorithm
*/
void printResult(struct List *list) {
	int * classes = getClasses(list);
	int i;
        int initialValue = 0;
	int maxValue = getMaxValue(list);
	int amountOfClasses = getAmountOfClasses(maxValue, list->size);
	int oldValue = initialValue;

	for(i=0;i<list->size-1;i++) {
		int newValue = list->data[i];
		printf("Data Set %d to %d\n\n", oldValue, newValue);
		int *crossedClasses = getCrossedClasses(oldValue,newValue,classes,amountOfClasses );
		oldValue = newValue;
		printf("\n");
	}

}

/*
	converts a given string to a double
*/
double convertStringToDouble(char * str) {
	int length = strlen(str);
	int i;

	for(i=0;i<length;i++) {
		if(str[i] == ',') {
			str[i] = '.';
		}
	}

	return atof(str);
}


int main(int argc, char **argv) {
	
	List *list = malloc(sizeof *list);
	FILE *CSV;
	double data;
	char dataString[1024];

	if(argc < 2) {
      		fprintf(stderr, "Verwendung : %s datei.csv\n", *argv);
      		return EXIT_FAILURE;
   	}

   	CSV = fopen(argv[1], "r");
   	if(NULL == CSV) {
      		fprintf(stderr, "Fehler beim Oeffnen ...\n");
      		return EXIT_FAILURE;
   	}
	
	int i=0;
	while(fgets(dataString, 1024, CSV) != NULL) {
		list->data[i] = convertStringToDouble(dataString);
		i++;
	}
	
	list->size = i+1;
	printResult(list);


   return EXIT_SUCCESS;
}




















