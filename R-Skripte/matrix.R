
# Matrixtypen

# speicher die Werte 1 bis 20 ein
my.data <- 1:20

# ausgabe
my.data 

# matrix() funktion
# 20 Eintr�ge, 4 * 5 Matrix
A <- matrix(my.data, 4, 5)
A

# row = 1 col = 2
A[1,2]

# Reihenweise speichern
B <- matrix(my.data, 4, 5, byrow = TRUE)
B

# Zweite funktion: rbind()
# Reihenweise

r1 <- c("What", "a", "day")
r2 <- c("A", "B", "C")
r3 <- c(1,2,3)


C <- rbind(r1, r2, r3)
# ausgabe
C

# cbind()
# save the data by colm

c1 <- c("What", "a", "day")
c2 <- c("A", "B", "C")
c3 <- c(1,2,3)

D <- cbind(r1, r2, r3)
# ausgabe
D