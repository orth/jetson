import keras.backend as K

config = K.tf.ConfigProto()
config.gpu_options.allow_growth = True
session = K.tf.Session(config=config)

#reading data
import pandas as pd

q1 = 0
q2 = 1
q3 = 2
q4 = 3
q5 = 4
q6 = 5

data = pd.read_csv('data.csv', usecols=[q2,q3,q4,q5,q6,q1], header=None, delimiter=';')

#filling missinf fields with zeroes
data = data.fillna(0)

#collumns -> rows, rows -> columns
x = data.T

#creating trainining data for results
y = pd.DataFrame([[0,0,1],
		  [0,0,1],
		  [0,0,1],
		  [0,1,0],
		  [0,1,1],
		  [1,0,0]])

#print(y)
#dataset = data.T


rows, cols = x.shape

#x = dataset[:,0:cols-1]
#y = dataset[:,cols-1]

#model
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Dropout
from keras import optimizers

model = Sequential()

#model.add(Dense(128, input_dim=cols, activation='sigmoid'))
#model.add(Dense(64, input_dim=cols, activation='sigmoid'))
#model.add(Dense(32, input_dim=cols, activation='sigmoid'))
#model.add(Dense(8, input_dim=cols, activation='sigmoid'))
#model.add(Dense(8, input_dim=cols, activation='sigmoid'))
#model.add(Dense(4, input_dim=cols, activation='sigmoid'))
#model.add(Dense(1, activation='sigmoid'))

model.add(Dense(256, input_dim=cols, kernel_initializer='uniform', activation='tanh'))
model.add(Dropout(0.5))
model.add(Dense(128, input_dim=cols, kernel_initializer='uniform', activation='tanh' ))
model.add(Dropout(0.5))
model.add(Dense(64, kernel_initializer='uniform', activation='tanh'))
model.add(Dropout(0.5))
model.add(Dense(32, kernel_initializer='uniform', activation='tanh'))
model.add(Dropout(0.5))
model.add(Dense(16, kernel_initializer='uniform', activation='tanh'))
#model.add(Dropout(0.5))
#model.add(Dense(8, kernel_initializer='uniform', activation='tanh'))
#model.add(Dropout(0.5))
model.add(Dense(3, init='uniform', activation='sigmoid'))

sgd = optimizers.SGD(lr=0.1, decay=1e-6,  momentum=0.9, nesterov=True)
model.compile(loss='mean_squared_error', optimizer=sgd)

#from keras.utils import plot_model
#plot_model(model, to_file='model.png')


#adam = optimizers.Adam(lr=0.01)
#model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])

hist = model.fit(x, y, epochs=500, batch_size=6, shuffle=True)

#evaluation
#scores = model.evaluate(x,y, batch_size=4)
#print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

#prediction

predictData = pd.read_csv('data.csv', usecols=[q1], header=None, delimiter=';')
predictData = predictData.fillna(0)
predictData = predictData.T

scores2 = model.predict(predictData)
print("\nPrediction: ")
print(scores2)

#saving learning history
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.plot(hist.history["loss"])
#plt.plot(hist.history["acc"])
plt.title("model loss")
plt.ylabel("loss")
plt.xlabel("epoch")
plt.legend(["train", "acc"], loc="upper left")
plt.savefig('historyPlot.png')

