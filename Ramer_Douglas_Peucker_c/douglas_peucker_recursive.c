/*
* Author Erlandas Miezys
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include "datastructures.h"


// prepares data for douglas peucker algorithm and allocates unified memory
void douglaspeuckerdataprep(bmzlist** bmzdata, pointlist** plist, int n){
	size_t i,j;
	for(i=0; i<n;i++){
		size_t size = bmzdata[i]->size;
		//double d = 0.0;
		for(j=0; j<size;j++){
			plist[i]->points[j].x = j;
			plist[i]->points[j].y = bmzdata[i]->data[j];
		


		}
		plist[i]->size = size;
	}

}

char * replace_char(char * input, char find, char replace) {
    char* output = (char*)malloc(strlen(input));

    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == find) output[i] = replace;
        else output[i] = input[i];
    }

    output[strlen(input)] = '\0';

    return output;
}

char* getFieldNumber(char* line, int num){
    char* temp;

    for (temp = strtok(line, ";"); temp && *temp; temp = strtok(NULL, ";\n")){
        if (!--num){
            if(isdigit(*temp)){
                temp = replace_char(temp, ',', '.');
                return temp;
            }
		}
        }
    return "X";
}

// Returns the desired column of the .csv file, it also filters the zero values out
bmzlist* getcolumndata(char* filepath, int column){
	bmzlist* data = (bmzlist*)malloc(sizeof(bmzlist));
	data->size = 0;

	for(int i = 0; i < data->size; i++){
        data->data[i] = 0;
    }

	FILE* stream = fopen(filepath, "r");
	if(stream == NULL){
		printf("Error reading file\n");
		}
	char line[LINE_SIZE];
	int start = 0;
	int count = 0;
	double hightemp = 0.0;

	while (fgets(line, LINE_SIZE, stream)){
        if(start != STARTING_POINT){
            start++;
        }else{
            char *endptr;
            char* tmp = strdup(line);
            char* returnTmp = getFieldNumber(tmp, column);
            if(strcmp(returnTmp, "X") != 0){
                // STRING TO DOUBLE CONVERSION
                double valueTmp = strtod(returnTmp, &endptr);
                 //CHECK IF valueTmp IS ZERO
                if(!(valueTmp == 0.0)){
                    if(valueTmp > hightemp && valueTmp < MAX_SPEED){
                        // SET HIGHEST DATA VALUE
                        hightemp = valueTmp;
                    }
                    if(valueTmp < MAX_SPEED){
                        data->size++;
                        data->data[count] = valueTmp;
                        count++;
                    }

                }
             }
            free(tmp);
        }
	}
	data->hightemp = hightemp;
	return data;
}


double perpendiculardistance(point pt, point pfirst, point plast){
	
	double dx = plast.x - pfirst.x;
	double dy = plast.y - pfirst.y;
	
	//normalise
	double mag = pow(pow(dx, 2.0) + pow(dy, 2.0), 0.5);
	if (mag > 0.0){
		dx /= mag;
		dy /= mag;
		}
	
	double pvx = pt.x - pfirst.x;
	double pvy = pt.y - pfirst.y;
	
	//get dot product
	double pvdot = dx * pvx + dy * pvy;
	
	//scale line direction vector
	double dsx = pvdot * dx;
	double dsy = pvdot * dy;
	
	//subtract this from pv
	double ax = pvx - dsx;
	double ay = pvy - dsy;
	
	return pow(pow(ax, 2.0) + pow(ay, 2.0), 0.5);
	
	}

void douglaspeuckerrecursive(pointlist* plist, double epsilon, pointlist* presult){
	
	if (plist->size < 2){
		printf("Not enough points to simplify\n");
		}
		
	//find the point with the maximum distance from line between start and end
	double dmax = 0.0;
	size_t index = 0;
	size_t end = plist->size - 1;
	size_t i;
	for(i=1; i<end; i++){
		
		double d = perpendiculardistance(plist->points[i], plist->points[0], plist->points[end]);
		if (d > dmax){
			index = i;
			dmax = d;
			}
		
		}
	if (dmax > epsilon)
	{
		size_t firstlinesize = index + 1;
		size_t lastlinesize = plist->size - index;
		
		
		pointlist* recresults1 = (pointlist*) malloc (sizeof(pointlist));
		recresults1->size = 0;
		pointlist* recresults2 = (pointlist*) malloc (sizeof(pointlist));
		recresults2->size = 0;
		
		pointlist* firstline = (pointlist*) malloc (sizeof(pointlist));
		pointlist* lastline = (pointlist*) malloc (sizeof(pointlist));
		
		for(i=0; i<firstlinesize; i++){
			firstline->points[i] = plist->points[i];
			}
			firstline->size = firstlinesize;
			
		size_t j = plist->size - lastlinesize;
		for(i=0; i<lastlinesize; i++){
			lastline->points[i] = plist->points[j];
			j++;
			}
			lastline->size = lastlinesize;
			
		
		douglaspeuckerrecursive(firstline, epsilon, recresults1);
		douglaspeuckerrecursive(lastline, epsilon, recresults2);
		

		
		for(i=0; i<recresults1->size; i++){
			presult->points[i] = recresults1->points[i];
			}
			presult->size = recresults1->size;
			
		size_t begin = presult->size;
		size_t ende = begin + recresults2->size-1;
		j=1;
		for(i=begin; i<ende; i++){
			presult->points[i] = recresults2->points[j];
			j++;
			}
			presult->size = ende;
		}
	else
	{
			presult->size = 2;
			presult->points[0] = plist->points[0];
			presult->points[1] = plist->points[end];
	}
}

int main(){
	char* filepath = "testdata.csv";
	int columnstoprocess = 1;
	bmzlist** list = (bmzlist**) malloc (sizeof(bmzlist*) * columnstoprocess);
	size_t i;
	// allocating memory for structure in unified memory and assigning address
	for(i=0; i<columnstoprocess; i++){
		list[i] = (bmzlist*) malloc (sizeof(bmzlist));
		*list[i] = *getcolumndata(filepath, i+1);
	}
	
	

	pointlist** plist = (pointlist**) malloc (sizeof(pointlist) * columnstoprocess);
	for(i=0; i<columnstoprocess; i++){
		plist[i] = (pointlist*) malloc (sizeof(pointlist));
		}
	douglaspeuckerdataprep(list, plist, columnstoprocess);
	
	
	
	
	pointlist** pointlistout = (pointlist**) malloc (sizeof(pointlist));
	for(i=0; i<columnstoprocess; i++){
		pointlistout[i] = (pointlist*) malloc (sizeof(pointlist));
		pointlistout[i]->size = 0;
		}
	
	
	
	/*pointlist* p = (pointlist*) malloc (sizeof(pointlist) * 10);
	p->points[0].x = 0;
	p->points[0].y = 0;
	p->points[1].x = 1;
	p->points[1].y = 0.1;
	p->points[2].x = 2;
	p->points[2].y = -0.1;
	p->points[3].x = 3;
	p->points[3].y = 5;
	p->points[4].x = 4;
	p->points[4].y = 6;
	p->points[5].x = 5;
	p->points[5].y = 7;
	p->points[6].x = 6;
	p->points[6].y = 8;
	p->points[7].x = 7;
	p->points[7].y = 9;
	p->points[8].x = 8;
	p->points[8].y = 9;
	p->points[9].x = 9;
	p->points[9].y = 9;
	p->size = 10;
	* */

	douglaspeuckerrecursive(plist[0], 1.0, pointlistout[0]);

		
	printf("Original size: %zu\n", plist[0]->size);
	printf("After peucker size: %zu\n", pointlistout[0]->size);
	printf("Saving results to .csv\n");
	FILE* file = fopen("peucker_results_recursive.csv", "w");
	for(i=0; i<pointlistout[0]->size; i++){
		fprintf(file, "%f\n", pointlistout[0]->points[i].y);
		}
	fclose(file);
	printf("Saving results to .csv finished\n");

	FILE* gnuplot1 = popen("gnuplot -persistent", "w");
	fprintf(gnuplot1, "plot '-' with lines\n");
	for(i=0; i<plist[0]->size; i++){
		fprintf(gnuplot1, "%i %f\n", i, plist[0]->points[i].y);
		}
	fprintf(gnuplot1, "e\n");
	fflush(gnuplot1);	

	// plotting data. requires gnuplot to be installed
	FILE* gnuplot = popen("gnuplot -persistent", "w");
	fprintf(gnuplot, "plot '-' with lines\n");
	for(i=0; i<pointlistout[0]->size; i++){
		fprintf(gnuplot, "%i %f\n", i, pointlistout[0]->points[i].y);
		}
	fprintf(gnuplot, "e\n");
	fflush(gnuplot);
	return 0;
	}
