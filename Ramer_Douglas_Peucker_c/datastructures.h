/*
* Author Erlandas Miezys
*/

#define LINE_SIZE 1024
#define STARTING_POINT 8
#define MAX_SPEED 100
#define ARRAY_SIZE 20000



typedef struct maxpoint{
	double dmax;
	size_t index;
	}maxpoint;

typedef struct check{
	bool avoid[ARRAY_SIZE];
	size_t size;
	}check;

typedef struct point{
	double x;
	double y;
	}point;
	
typedef struct stackelement{
	size_t startindex;
	size_t lastindex;
	}stackelement;
	
typedef struct stack{
	stackelement element[ARRAY_SIZE];
	size_t size;
	int top;
	}stack;
	
typedef struct pointlist{
	point points[20000];
	size_t size;
	}pointlist;
	
// used for "bereichsmittelwertzaehlung" algorithm
typedef struct bmzlist{
    int size;
    double data[ARRAY_SIZE];
    int hightemp;
}bmzlist;




