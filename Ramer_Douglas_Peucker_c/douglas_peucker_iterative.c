/*
* Author Erlandas Miezys
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "datastructures.h"



// prepares data for douglas peucker algorithm and allocates unified memory
void douglaspeuckerdataprep(bmzlist** bmzdata, pointlist** plist, int n){
	size_t i,j;
	for(i=0; i<n;i++){
		size_t size = bmzdata[i]->size;
		//double d = 0.0;
		for(j=0; j<size;j++){
			plist[i]->points[j].x = j;
			plist[i]->points[j].y = bmzdata[i]->data[j];
			//printf("list: %f\n",plist[i]->points[j].y );


		}
		plist[i]->size = size;
	}

}

char * replace_char(char * input, char find, char replace) {
    char* output = (char*)malloc(strlen(input));

    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == find) output[i] = replace;
        else output[i] = input[i];
    }

    output[strlen(input)] = '\0';

    return output;
}

char* getFieldNumber(char* line, int num){
    char* temp;

    for (temp = strtok(line, ";"); temp && *temp; temp = strtok(NULL, ";\n")){
        if (!--num){
            if(isdigit(*temp) != 0){
                temp = replace_char(temp, ',', '.');
                return temp;
            }
		}
        }
    return "X";
}

// Returns the desired column of the .csv file, it also filters the zero values out
bmzlist* getcolumndata(char* filepath, int column){
	bmzlist* data = (bmzlist*)malloc(sizeof(bmzlist));
	data->size = 0;

	for(int i = 0; i < data->size; i++){
        data->data[i] = 0;
    }

	FILE* stream = fopen(filepath, "r");
	if(stream == NULL){
		printf("Error reading file\n");
		}
	char line[LINE_SIZE];
	int start = 0;
	int count = 0;
	double hightemp = 0.0;

	while (fgets(line, LINE_SIZE, stream)){
        if(start != STARTING_POINT){
            start++;
        }else{
            char *endptr;
            char* tmp = strdup(line);
            char* returnTmp = getFieldNumber(tmp, column);
            if(strcmp(returnTmp, "X") != 0){
                // STRING TO DOUBLE CONVERSION
                double valueTmp = strtod(returnTmp, &endptr);
                 //CHECK IF valueTmp IS ZERO
                if(!(valueTmp == 0.0)){
                    if(valueTmp > hightemp && valueTmp < MAX_SPEED){
                        // SET HIGHEST DATA VALUE
                        hightemp = valueTmp;
                    }
                    if(valueTmp < MAX_SPEED){
                        data->size++;
                        data->data[count] = valueTmp;
                        count++;
                    }

                }
             }
            free(tmp);
        }
	}
	data->hightemp = hightemp;
	return data;
}

// calculates distance betven points
double perpendiculardistance(point pt, point pfirst, point plast){
	double dx = plast.x - pfirst.x;
	double dy = plast.y - pfirst.y;
	
	//normalise
	double mag = pow(pow(dx, 2.0) + pow(dy, 2.0), 0.5);
	if (mag > 0.0){
		dx /= mag;
		dy /= mag;
		}
	
	double pvx = pt.x - pfirst.x;
	double pvy = pt.y - pfirst.y;
	
	//get dot product
	double pvdot = dx * pvx + dy * pvy;
	
	//scale line direction vector
	double dsx = pvdot * dx;
	double dsy = pvdot * dy;
	
	//subtract this from pv
	double ax = pvx - dsx;
	double ay = pvy - dsy;
	
	return pow(pow(ax, 2.0) + pow(ay, 2.0), 0.5);
	
	}

stackelement peek(stack stk){
	return stk.element[stk.top];
	}

bool isfull(stack* stk) {
	if(stk->size >= ARRAY_SIZE){
		return true;}
		else{
			return false;
			}
}

bool isempty(stack* stk) {

   if(stk->top == -1)
      return true;
   else
      return false;
}

void push(stack* stk, stackelement element) {

   if(isfull(stk) == false) {
      stk->top = stk->top + 1;   
      stk->element[stk->top] = element;
      stk->size += 1; 
   } else {
      printf("Could not insert data, Stack is full.\n");
   }
}

void pop(stack* stk) {
   
   if(isempty(stk) == false) {
      stk->top -= 1;   
      stk->size -= 1;
   } else {
      printf("Could not retrieve data, Stack is empty.\n");
   }
}

check avoid(pointlist* plist, size_t startindex, size_t lastindex, float epsilon){
		stack stk;
		stk.size = 0;
		stk.top = -1;
		check ch;
		size_t i;
		for(i=0; i<ARRAY_SIZE; i++){
			ch.avoid[i] = true;
			}
		stackelement element;
		element.startindex = startindex;
		element.lastindex = lastindex;
		push(&stk, element);
		
		size_t globalstartindex = startindex;
		
		while(stk.size > 0){
				startindex = peek(stk).startindex;
				lastindex = peek(stk).lastindex;
				pop(&stk);
				
				float dmax = 0.0;
				int index = startindex;
				
				for(i=index+1; i < lastindex; ++i){
					//printf("lastindex: %i\n", lastindex);
					if(ch.avoid[i - globalstartindex] == true){
						//printf("i: %i\n", i);
						double d = perpendiculardistance(plist->points[i], plist->points[startindex], plist->points[lastindex]);
						
						if(d > epsilon){
							index = i;
							dmax = d;
							}
						}
					}
				
				if (dmax > epsilon){
					stackelement selement;
					selement.startindex = startindex;
					selement.lastindex = index;
					push(&stk,selement);
					stackelement selement2;
					selement2.startindex = index;
					selement2.lastindex = lastindex;
					push(&stk, selement2);
					}
				else{
					for(i=startindex+1; i < lastindex; ++i){
						ch.avoid[i - globalstartindex] = false;
						ch.size += 1;
						}
					}
			}
		
		return ch;
	}


void douglaspeuckeriterative(pointlist* plist, double epsilon, pointlist* presult){
	
	check bitarray = avoid(plist, 0, plist->size-1, epsilon);
	size_t i, j=0;
	for(i=0; i<plist->size; i++){
		if(bitarray.avoid[i] == true){
			presult->points[j] = plist->points[i];
			presult->size += 1;
			j++;
			}
		}
		
}


int main(){
	char* filepath = "testdata.csv";
	int columnstoprocess = 1;
	bmzlist** list = (bmzlist**) malloc (sizeof(bmzlist*) * columnstoprocess);
	size_t i;
	// allocating memory for structure in unified memory and assigning address
	for(i=0; i<columnstoprocess; i++){
		list[i] = (bmzlist*) malloc (sizeof(bmzlist));
		*list[i] = *getcolumndata(filepath, i+1);
	}
	
	

	pointlist** plist = (pointlist**) malloc (sizeof(pointlist) * columnstoprocess);
	for(i=0; i<columnstoprocess; i++){
		plist[i] = (pointlist*) malloc (sizeof(pointlist));
		}
	douglaspeuckerdataprep(list, plist, columnstoprocess);
	
	
	
	
	pointlist** pointlistout = (pointlist**) malloc (sizeof(pointlist));
	for(i=0; i<columnstoprocess; i++){
		pointlistout[i] = (pointlist*) malloc (sizeof(pointlist));
		pointlistout[i]->size = 0;
		}
	/*
	pointlist* p = (pointlist*) malloc (sizeof(pointlist) * 10);
	p->points[0].x = 0;
	p->points[0].y = 0;
	p->points[1].x = 1;
	p->points[1].y = 0.1;
	p->points[2].x = 2;
	p->points[2].y = -0.1;
	p->points[3].x = 3;
	p->points[3].y = 5;
	p->points[4].x = 4;
	p->points[4].y = 6;
	p->points[5].x = 5;
	p->points[5].y = 7;
	p->points[6].x = 6;
	p->points[6].y = 8;
	p->points[7].x = 7;
	p->points[7].y = 9;
	p->points[8].x = 8;
	p->points[8].y = 9;
	p->points[9].x = 9;
	p->points[9].y = 9;
	p->size = 10;
	 */

	douglaspeuckeriterative(plist[0], 1.0, pointlistout[0]);
	//douglaspeuckeriterative(p, 1.0, pointlistout[0]);
/*
	for(i=0; i<pointlistout[0]->size; i++){
		printf("%f and %f\n",pointlistout[0]->points[i].x, pointlistout[0]->points[i].y);
		}
*/	
	printf("Original size: %zu\n", plist[0]->size);
	printf("After peucker size: %zu\n", pointlistout[0]->size);
	printf("Saving results to .csv\n");
	FILE* file = fopen("peucker_results_iterative.csv", "w");
	for(i=0; i<pointlistout[0]->size; i++){
		fprintf(file, "%f\n", pointlistout[0]->points[i].y);
		}
	fclose(file);
	printf("Saving results to .csv finished\n");
	
	// plotting data. requires gnuplot to be installed
	FILE* gnuplot = popen("gnuplot -persistent", "w");
	fprintf(gnuplot, "plot '-' with lines\n");
	for(i=0; i<pointlistout[0]->size; i++){
		fprintf(gnuplot, "%i %f\n", i, pointlistout[0]->points[i].y);
		}
	fprintf(gnuplot, "e\n");
	fflush(gnuplot);
	return 0;
	}

